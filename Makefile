CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

prepare:
	bundle install

test:
	bundle exec jekyll serve

update:
	mkdir tmp; cd tmp; git clone https://gitlab.com/auksys/auksys_build.git
	ruby tmp/auksys_build/auksys-build --instructions
	mv INSTRUCTIONS.MD _includes/documentation/installation/build_manual.md
	rm -rf tmp
	cd _posts/auksys.posts.cberger.net; git pull; git checkout stable

check-local:
	docker run --rm --network="host" -it -u ${CURRENT_UID}:${CURRENT_GID} ghcr.io/linkchecker/linkchecker:latest http://127.0.0.1:4000/

check-remote:
	docker run --rm --network="host" -it -u ${CURRENT_UID}:${CURRENT_GID} ghcr.io/linkchecker/linkchecker:latest https://auksys.org

fetch:
	git clone https://oauth2:glpat-wXyJ9WEy4wxQALysSBHs@gitlab.com/cyrilleberger/auksys.posts.cberger.net _posts/auksys.posts.cberger.net
