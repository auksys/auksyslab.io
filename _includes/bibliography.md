auKsys is described in the following publications:

* [Leveraging active queries in collaborative robotic mission planning](/assets/publications/2024_aq_ir.pdf). C Berger, P Doherty, P Rudol, M Wzorek. *Intelligence & Robotics*. 2024.
* [RGS⊕: RDF graph synchronization for collaborative robotics](/assets/publications/2023_rgsplus_aamas.pdf). C Berger, P Doherty, P Rudol, M Wzorek. *Autonomous Agents and Multi-Agent Systems*. 2023.
* [Hastily formed knowledge networks and distributed situation awareness for collaborative robotics](/assets/publications/2021_hfkn_ais.pdf). P Doherty, C Berger, P Rudol, M Wzorek. *Autonomous Intelligent Systems*. 2021.

If you are using auKsys in any of your project, you are encouraged to reference one of the publication above.