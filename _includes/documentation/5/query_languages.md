* [SQL](/documentation/5/query_languages/sql/)
* [SPARQL](/documentation/5/query_languages/sparql/)
* [kdQL](/documentation/5/query_languages/kdql/)
* [krQL](/documentation/5/query_languages/krql/)
