* `delegation/send_cfp` is used by the delegation module to broadcast a call for proposals by other agent.
* `delegation/send_offer` is used by the delegation module to answer a CFP with a cost proposal.
* `delegation/send_proposal_acceptance` is used by the delegation module to select a proposal.
* `delegation/send_execution_acceptance` is used by the delegation module to confirm that the proposal acceptance has been received and that the agent will execute the mission.
* `delegation/cancel_acceptance` is used to cancel the acceptance, this is send if no `execution acceptance` is received.