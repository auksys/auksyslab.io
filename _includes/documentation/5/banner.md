<p class="auksys-documentation-version-banner">This page is part of the documentation of <b>auKsys/5</b></p>

**WARNING *auKsys/5* has been released as stable, but this documentation has not been fully updated with changes since *auKsys/4*. In case of questions or mistakes, you can use the [support](https://gitlab.com/auksys/support) project.**