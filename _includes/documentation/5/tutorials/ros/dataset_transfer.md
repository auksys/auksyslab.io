The [Dataset Transfer](/documentation/5/tutorials/ros/dataset_transfer/) tutorial presents how to exchange large dataset of sensory data between two platforms.
