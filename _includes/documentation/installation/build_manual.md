This page contains instructions for building `auKsys` manually. The content of this page is generated by `auksys-build`.
The instructions are available for the `stable`, `legacy/4` and `dev/5` distributions.

stable
===============

System packages
---------------

### Ubuntu 24.04 LTS

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev libmapnik-dev postgis libcunit1-dev postgis ruby-dev ruby-rspec postgresql-plpython3-16 libgeographiclib-dev geographiclib-tools pybind11-dev libeigen3-dev cmake libfmt-dev qt6-declarative-dev qt6-positioning-dev qt6-quick3d-dev qt6-3d-dev ruby-dev libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ qml6-module-qt3d-extras qml6-module-qtquick-layouts qml6-module-qtquick-controls qml6-module-qtquick qml6-module-qtqml-workerscript qml6-module-qtquick-templates qml6-module-qtquick-window qml6-module-qtquick-shapes qml6-module-qt-labs-qmlmodels qml6-module-qt-labs-settings qml6-module-qtquick-dialogs qt6-svg-dev qml6-module-qtcore qml6-module-qt5compat-graphicaleffects liboctomap-dev
gem install rice
```

### Debian GNU/Linux trixie/sid

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev libmapnik-dev postgis libcunit1-dev postgis ruby-dev ruby-rspec postgresql-plpython3-16 libgeographiclib-dev geographiclib-tools pybind11-dev libeigen3-dev cmake qt6-declarative-dev qt6-positioning-dev qt6-quick3d-dev qml6-module-qt3d-extras qt6-3d-dev ruby-dev libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ qml6-module-qtquick-layouts qml6-module-qtquick-controls qml6-module-qtquick qml6-module-qtqml-workerscript qml6-module-qtquick-templates qml6-module-qtquick-window qml6-module-qtquick-shapes qml6-module-qt-labs-qmlmodels qml6-module-qt-labs-settings qml6-module-qtquick-dialogs qt6-svg-dev qml6-module-qtcore qml6-module-qt5compat-graphicaleffects liboctomap-dev
gem install rice
```

auKsys packages
---------------

The following instructions will build auKsys in the `$HOME/auksys`, this directory can be adjusted.

```bash
export AUKSYS_ROOT=$HOME/auksys
mkdir -p $AUKSYS_ROOT/src
mkdir -p $AUKSYS_ROOT/build
```

### Build cext

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/cext cext
mkdir -p $AUKSYS_ROOT/build/cext
cd $AUKSYS_ROOT/build/cext
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/cext -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ptc

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/ptc ptc
mkdir -p $AUKSYS_ROOT/build/ptc
cd $AUKSYS_ROOT/build/ptc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ptc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ag

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/ag ag
mkdir -p $AUKSYS_ROOT/build/ag
cd $AUKSYS_ROOT/build/ag
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ag -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build parc

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/parc parc
mkdir -p $AUKSYS_ROOT/build/parc
cd $AUKSYS_ROOT/build/parc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/parc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build tasks_machine

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/tasks_machine tasks_machine
mkdir -p $AUKSYS_ROOT/build/tasks_machine
cd $AUKSYS_ROOT/build/tasks_machine
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/tasks_machine -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pybind11-qt

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/pybind11-qt pybind11-qt
mkdir -p $AUKSYS_ROOT/build/pybind11-qt
cd $AUKSYS_ROOT/build/pybind11-qt
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pybind11-qt -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cyqlops

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/cyqlops Cyqlops
mkdir -p $AUKSYS_ROOT/build/Cyqlops
cd $AUKSYS_ROOT/build/Cyqlops
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cyqlops -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build euclid

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/euclid euclid
mkdir -p $AUKSYS_ROOT/build/euclid
cd $AUKSYS_ROOT/build/euclid
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/euclid -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cartography

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/cyloncore/Cartography Cartography
mkdir -p $AUKSYS_ROOT/build/Cartography
cd $AUKSYS_ROOT/build/Cartography
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cartography -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build TstML

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/auksys/TstML TstML
mkdir -p $AUKSYS_ROOT/build/TstML
cd $AUKSYS_ROOT/build/TstML
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/TstML -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build knowL

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/auksys/knowL knowL
mkdir -p $AUKSYS_ROOT/build/knowL
cd $AUKSYS_ROOT/build/knowL
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/knowL -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build kDB

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/auksys/kdb.git kDB
mkdir -p $AUKSYS_ROOT/build/kDB
cd $AUKSYS_ROOT/build/kDB
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/kDB -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pralin

```bash
cd $AUKSYS_ROOT/src
git clone -b stable https://gitlab.com/auksys/pralin pralin
mkdir -p $AUKSYS_ROOT/build/pralin
cd $AUKSYS_ROOT/build/pralin
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pralin -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

dev/5
===============

System packages
---------------

### Ubuntu 24.04 LTS

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev libmapnik-dev postgis libcunit1-dev postgis ruby-dev ruby-rspec postgresql-plpython3-16 libgeographiclib-dev geographiclib-tools pybind11-dev libeigen3-dev cmake libfmt-dev qt6-declarative-dev qt6-positioning-dev qt6-quick3d-dev qt6-3d-dev ruby-dev libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ qml6-module-qt3d-extras qml6-module-qtquick-layouts qml6-module-qtquick-controls qml6-module-qtquick qml6-module-qtqml-workerscript qml6-module-qtquick-templates qml6-module-qtquick-window qml6-module-qtquick-shapes qml6-module-qt-labs-qmlmodels qml6-module-qt-labs-settings qml6-module-qtquick-dialogs qt6-svg-dev qml6-module-qtcore qml6-module-qt5compat-graphicaleffects liboctomap-dev
gem install rice
```

### Debian GNU/Linux trixie/sid

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev libmapnik-dev postgis libcunit1-dev postgis ruby-dev ruby-rspec postgresql-plpython3-16 libgeographiclib-dev geographiclib-tools pybind11-dev libeigen3-dev cmake qt6-declarative-dev qt6-positioning-dev qt6-quick3d-dev qml6-module-qt3d-extras qt6-3d-dev ruby-dev libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ qml6-module-qtquick-layouts qml6-module-qtquick-controls qml6-module-qtquick qml6-module-qtqml-workerscript qml6-module-qtquick-templates qml6-module-qtquick-window qml6-module-qtquick-shapes qml6-module-qt-labs-qmlmodels qml6-module-qt-labs-settings qml6-module-qtquick-dialogs qt6-svg-dev qml6-module-qtcore qml6-module-qt5compat-graphicaleffects liboctomap-dev
gem install rice
```

auKsys packages
---------------

The following instructions will build auKsys in the `$HOME/auksys`, this directory can be adjusted.

```bash
export AUKSYS_ROOT=$HOME/auksys
mkdir -p $AUKSYS_ROOT/src
mkdir -p $AUKSYS_ROOT/build
```

### Build cext

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/cext cext
mkdir -p $AUKSYS_ROOT/build/cext
cd $AUKSYS_ROOT/build/cext
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/cext -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ptc

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/ptc ptc
mkdir -p $AUKSYS_ROOT/build/ptc
cd $AUKSYS_ROOT/build/ptc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ptc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ag

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/ag ag
mkdir -p $AUKSYS_ROOT/build/ag
cd $AUKSYS_ROOT/build/ag
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ag -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build parc

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/parc parc
mkdir -p $AUKSYS_ROOT/build/parc
cd $AUKSYS_ROOT/build/parc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/parc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build tasks_machine

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/tasks_machine tasks_machine
mkdir -p $AUKSYS_ROOT/build/tasks_machine
cd $AUKSYS_ROOT/build/tasks_machine
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/tasks_machine -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pybind11-qt

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/pybind11-qt pybind11-qt
mkdir -p $AUKSYS_ROOT/build/pybind11-qt
cd $AUKSYS_ROOT/build/pybind11-qt
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pybind11-qt -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cyqlops

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/cyqlops Cyqlops
mkdir -p $AUKSYS_ROOT/build/Cyqlops
cd $AUKSYS_ROOT/build/Cyqlops
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cyqlops -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build euclid

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/euclid euclid
mkdir -p $AUKSYS_ROOT/build/euclid
cd $AUKSYS_ROOT/build/euclid
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/euclid -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cartography

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/cyloncore/Cartography Cartography
mkdir -p $AUKSYS_ROOT/build/Cartography
cd $AUKSYS_ROOT/build/Cartography
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cartography -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build TstML

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/2 https://gitlab.com/auksys/TstML TstML
mkdir -p $AUKSYS_ROOT/build/TstML
cd $AUKSYS_ROOT/build/TstML
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/TstML -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build knowL

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/3 https://gitlab.com/auksys/knowL knowL
mkdir -p $AUKSYS_ROOT/build/knowL
cd $AUKSYS_ROOT/build/knowL
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/knowL -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build kDB

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/5 https://gitlab.com/auksys/kdb.git kDB
mkdir -p $AUKSYS_ROOT/build/kDB
cd $AUKSYS_ROOT/build/kDB
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/kDB -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pralin

```bash
cd $AUKSYS_ROOT/src
git clone -b dev/3 https://gitlab.com/auksys/pralin pralin
mkdir -p $AUKSYS_ROOT/build/pralin
cd $AUKSYS_ROOT/build/pralin
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pralin -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

legacy/4
===============

System packages
---------------

### Ubuntu 22.04

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev postgis qtbase5-dev qt5-qmake libcunit1-dev postgis ruby postgresql-plpython3-14 libgeographic-dev geographiclib-tools python3-dev pybind11-dev qtpositioning5-dev qt3d5-dev libeigen3-dev qttools5-dev-tools cmake libfmt-dev qtdeclarative5-private-dev qtbase5-private-dev ruby-dev ruby-rspec libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ liboctomap-dev qml-module-qt3d qml-module-qtquick-layouts qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qtqml-workerscript2 qml-module-qtquick-templates2 qml-module-qtquick-window2 qml-module-qtquick-shapes qml-module-qt-labs-qmlmodels qml-module-qt-labs-settings qml-module-qtquick-dialogs qml-module-qtgraphicaleffects qml-module-qtcharts
gem install rice
```

### Debian GNU/Linux trixie/sid

The following commands will install the sytem dependencies:
```bash
apt install postgresql-server-dev-all libpq-dev libgdal-dev postgis qtbase5-dev qt5-qmake libcunit1-dev postgis ruby postgresql-plpython3-14 libgeographic-dev geographiclib-tools pybind11-dev qtpositioning5-dev libeigen3-dev qttools5-dev-tools cmake libfmt-dev ruby-rspec qtdeclarative5-private-dev qtbase5-private-dev ruby-dev libyaml-cpp-dev libpaho-mqttpp-dev libpaho-mqtt-dev git cmake g++ liboctomap-dev
gem install rice
```

auKsys packages
---------------

The following instructions will build auKsys in the `$HOME/auksys`, this directory can be adjusted.

```bash
export AUKSYS_ROOT=$HOME/auksys
mkdir -p $AUKSYS_ROOT/src
mkdir -p $AUKSYS_ROOT/build
```

### Build cext

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/cext cext
mkdir -p $AUKSYS_ROOT/build/cext
cd $AUKSYS_ROOT/build/cext
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/cext -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ptc

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/ptc ptc
mkdir -p $AUKSYS_ROOT/build/ptc
cd $AUKSYS_ROOT/build/ptc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ptc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build ag

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/ag ag
mkdir -p $AUKSYS_ROOT/build/ag
cd $AUKSYS_ROOT/build/ag
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/ag -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build parc

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/parc parc
mkdir -p $AUKSYS_ROOT/build/parc
cd $AUKSYS_ROOT/build/parc
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/parc -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build tasks_machine

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/tasks_machine tasks_machine
mkdir -p $AUKSYS_ROOT/build/tasks_machine
cd $AUKSYS_ROOT/build/tasks_machine
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/tasks_machine -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pybind11-qt

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/pybind11-qt pybind11-qt
mkdir -p $AUKSYS_ROOT/build/pybind11-qt
cd $AUKSYS_ROOT/build/pybind11-qt
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pybind11-qt -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cyqlops

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/cyqlops Cyqlops
mkdir -p $AUKSYS_ROOT/build/Cyqlops
cd $AUKSYS_ROOT/build/Cyqlops
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cyqlops -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build euclid

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/euclid euclid
mkdir -p $AUKSYS_ROOT/build/euclid
cd $AUKSYS_ROOT/build/euclid
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/euclid -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build Cartography

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/cyloncore/Cartography Cartography
mkdir -p $AUKSYS_ROOT/build/Cartography
cd $AUKSYS_ROOT/build/Cartography
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/Cartography -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build TstML

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/1 https://gitlab.com/auksys/TstML TstML
mkdir -p $AUKSYS_ROOT/build/TstML
cd $AUKSYS_ROOT/build/TstML
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/TstML -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build knowL

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/2 https://gitlab.com/auksys/knowL knowL
mkdir -p $AUKSYS_ROOT/build/knowL
cd $AUKSYS_ROOT/build/knowL
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/knowL -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build kDB

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/4 https://gitlab.com/auksys/kdb.git kDB
mkdir -p $AUKSYS_ROOT/build/kDB
cd $AUKSYS_ROOT/build/kDB
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/kDB -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

### Build pralin

```bash
cd $AUKSYS_ROOT/src
git clone -b legacy/2 https://gitlab.com/auksys/pralin pralin
mkdir -p $AUKSYS_ROOT/build/pralin
cd $AUKSYS_ROOT/build/pralin
cmake -DCMAKE_BUILD_TYPE=Release $AUKSYS_ROOT/src/pralin -DCMAKE_INSTALL_PREFIX=$AUKSYS_ROOT/inst -DOPTIONAL_DEPENDENCIES_ARE_REQUIRED=ON
make -j8 install
```

