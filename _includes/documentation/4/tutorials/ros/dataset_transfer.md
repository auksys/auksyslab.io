The [Dataset Transfer](/documentation/4/tutorials/ros/dataset_transfer/) tutorial presents how to exchange large dataset of sensory data between two platforms.
