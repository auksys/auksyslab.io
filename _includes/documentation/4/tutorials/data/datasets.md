[Datasets](/documentation/4/tutorials/data/datasets/) is an introductions to datasets in kDB, it covers how to create them, change their properties and import/export datasets.
