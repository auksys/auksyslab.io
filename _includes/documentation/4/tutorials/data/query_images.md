The [query images](/documentation/4/tutorials/data/query_images/) tutorial shows how to query images, display them and process them using OpenCV.
