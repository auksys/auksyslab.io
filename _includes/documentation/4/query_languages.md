* [SQL](/documentation/4/query_languages/sql/)
* [SPARQL](/documentation/4/query_languages/sparql/)
* [kdQL](/documentation/4/query_languages/kdql/)
* [krQL](/documentation/4/query_languages/krql/)
