# Welcome to Jekyll!
#
# This config file is meant for settings that affect your whole blog, values
# which you are expected to set up once and rarely edit after that. If you find
# yourself editing this file very often, consider using Jekyll's data files
# feature for the data you need to update frequently.
#
# For technical reasons, this file is *NOT* reloaded automatically when you use
# 'bundle exec jekyll serve'. If you change this file, please restart the server process.

# Site settings
# These are used to personalize your new site. If you look in the HTML files,
# you will see them accessed via {{ site.title }}, {{ site.email }}, and so on.
# You can create any custom variable you would like, and they will be accessible
# in the templates via {{ site.myvariable }}.
title: "auKsys: autonomous knowledge system"
email: cyrille.berger@liu.se
description: >- # this means to ignore newlines until "baseurl:"
  This is the official website for the autonomous knowledge system (auKsys).
  It is a distributed knowledge system for multi-agents, with a focus on robotic agents.
  It can handle large sensory data, semantic information, automatic knowlege synchronisation,
  sharing sensor data and processing knowledge.
baseurl: "" # the subpath of your site, e.g. /blog
url: "https://auksys.org" # the base hostname & protocol for your site, e.g. http://example.com
# twitter_username: jekyllrb
# github_username:  jekyll

repository: https://gitlab.com/auksys/auksys.gitlab.io

logo: "/assets/images/logo-88x88.png"
masthead_title: "auKsys"
breadcrumbs: true
search: true

defaults:
  # _posts
  - scope:
      path: ""
      type: posts
    values:
      show_date: true
      layout: single
      share: true
      sidebar:
        nav: ["news"]
      permalink: /:categories/:title:output_ext
  # _posts
  - scope:
      path: "_posts/auksys.posts.cberger.net/*"
      type: posts
    values:
      category: blog
      sidebar:
        nav: ["blog"]
  # _documentation/4
  - scope:
      path: "_documentation/4/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/4"]
  - scope:
      path: "_documentation/4/libraries/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/4", "documentation/4/libraries"]
  - scope:
      path: "_documentation/4/query_languages/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/4", "documentation/4/query_languages"]
  - scope:
      path: "_documentation/4/ros/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/4", "documentation/4/ros"]
  - scope:
      path: "_documentation/4/tutorials/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/4", "documentation/4/tutorials"]
  # _documentation/5
  - scope:
      path: "_documentation/5/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5"]
  - scope:
      path: "_documentation/5/libraries/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/libraries"]
  - scope:
      path: "_documentation/5/libraries/kdb/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/libraries", "documentation/5/libraries/kdb"]
  - scope:
      path: "_documentation/5/libraries/pralin/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/libraries", "documentation/5/libraries/pralin"]
  - scope:
      path: "_documentation/5/programs/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/programs"]
  - scope:
      path: "_documentation/5/query_languages/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/query_languages"]
  - scope:
      path: "_documentation/5/ros/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/ros"]
  - scope:
      path: "_documentation/5/tutorials/*"
    values:
      sidebar:
        nav: ["documentation", "documentation/5", "documentation/5/tutorials"]
  # _documentation general
  - scope:
      path: ""
      type: documentation
    values:
      layout: single
      read_time: false
      author_profile: false
      share: false
      comments: false
      sidebar:
        nav: "documentation"
  # _pages
  - scope:
      path: "_pages"
      type: pages
    values:
      layout: single
      author_profile: true

# Collections
collections:
  documentation:
    output: true
    permalink: /:collection/:path/

include: 
  - _pages

exclude:
  - _external_posts

permalink: /:categories/:title/

# Build settings
markdown: kramdown
theme: minimal-mistakes-jekyll
plugins:
  - jekyll-feed
  - jekyll-redirect-from
  - jekyll-tabs
  - jekyll/scholar
  - jekyll-archives

minimal_mistakes_skin: "contrast"

scholar:
  style: _bibliography/ieee.csl
  bibliography_template: bibliography

head_scripts:
  - /assets/js/tabs.js

category_archive:
  type: liquid
  path: /categories/
tag_archive:
  type: liquid
  path: /tags/

# jekyll-archives
jekyll-archives:
  enabled: 'all'
  layouts:
    category: archive-taxonomy
    tag: archive-taxonomy
  permalinks:
    category: /categories/:name/
    tag: /tags/:name/