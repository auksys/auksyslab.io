---
title: Features
permalink: /features/
---

{% include module.html image_path="/assets/images/acquire.png" position="left" title="Acquire" description="auKsys integrates with many type of sensors (camera, lidar, gps...)." %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/process.png" position="right" title="Process" description="auKsys integrates with different processing algorithms to transform sensory data and extract high level semantic information." %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/store.png" position="left" title="Store" description="auKsys interfaces with a PostgreSQL database to efficiently store data as SQL or RDF. Allowing high performance storing of sensory data." %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/share.png" position="right" title="Share" description="auKsys includes algorithm for automatic synchronisation of knowledge graph, represented as RDF document. It also " %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/modular.png" position="left" title="Modular" description="auKsys follows a modular architecture, allowing integration at different levels. The main interface is a C++ API with a set of libraries. Partial bindings for Python and Ruby are also available. As well a ROS integration." %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/opensource.png" position="right" title="Open Source" description="auKsys is released Open Source under the <a href='https://opensource.org/license/mit/'>MIT license</a>. This make it possible to integrate auKsys in your own research and software stack ." %}

{% include seperator.html %}

You can follow our <a href='/documentation/installation/'>instalation instructions</a> to get started. You can read more in our <a href='/introduction/'>introduction</a>, <a href='/documentation/4/'>documentation (for auKsys/4)</a> or read about <a href='/research/'>our research</a>.
