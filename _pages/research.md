---
title: Research
permalink: /research/
classes: wide
---

Bibliography
------------

{% include bibliography.md %}

Past Research Projects
----------------------

*auKsys* is now primarly developed as an open source project, but received contributions from the following research projects in the past.

{% include module.html image_path="/assets/images/wasp_sweden_logo.png" position="left" title="WARA-PS 2017-2024" description="Public Safety and Security research is of increasing importance globally. The WASP Research Arena for Public Safety (WARA PS) promotes research in collaborative heterogenous agents and systems of systems - with the mission to keep society safe. [More information.](https://wasp-sweden.org/industrial-cooperation/research-arenas/wara-ps-public-safety/)" margin_bottom="0em" %}
{% include module.html image_path="/assets/images/wasp_platforms_400.jpg" position="right" title="" description="auKsys is been integrated as part of a team of UAVs, UGVs and USVs operating in rescue scenarios. kDB has been used to collect sensor data and to distribute 3D models of the environment, and point of interests. During the project the distributions mechanisms, for symbolic and non-symbolic data have been demonstrated. Active queries (scQL) were also used to show how to seemlessly acquire new information and re-use existing information." %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/vinnova-complete-green.svg" position="left" title="AuSSys: Autonomous Search System 2022-2024" description="This project aims to demonstrate some technologies to address the problem of vision fatigue in long-term missions caused by exhaustive repetition and its inaccuracies and limitations on the visible spectrum by using image-based Automatic Target Search System. By means of pattern recognition, perception, reasoning, decision-making and machine learning, it will combine available resources for situation assessment and possible delivery of survival-related supplies (medical, food etc.) on Search and Rescue missions." %}
{% include module.html image_path="/assets/images/aussys_results.png" image_width=400 position="right" title="" description="In this project, auKsys is used to coordinate the information between drones acquiring images in the sea, a ground operator and a cloud system used to detect anomalies.
A ground operator requests for anomalies in a given area of the environment, the request is transalted from an user-interface to an active query.
During the execution of the query, aukSys tries to re-use existing images, or otherwise, it will send drones to acquire new images.
The images are then processed in the cloud using machine learning. The results are stored in kDB and displayed in the user interface of the ground operator.
" %}

{% include seperator.html %}

{% include module.html image_path="/assets/images/elliit-logotyp.png" image_width=200 position="left" title="Collaborative robotics 202x-2024"  description="Dynamic and seamless interaction between collections of humans and robotic systems in achieving complex common goals and information exchange is an essential component in collaborative robotics. In this context, distributive situation awareness is essential for supporting collective intelligence in teams of robots and human agents where it can be used for both individual and collective decision support. [More information.](https://elliit.se/project/collaborative-robotics/)" %}

{% include module.html image_path="/assets/images/ssf-logo.svg" position="left" title="SymbiCloud 2016-2020" description="The focus of this research project was to investigate both the science and systems aspects of a smart systems enhancement of Robot-Assisted Hastily Formed Knowledge Networks where one embeds autonomous robotic systems, both ground and aerial, as players and team members serving as an integral part of the physical infrastructure. Additionally, the interaction practices category will be extended by automating much of the process of dynamic acquisition, structuring and abstraction of sensory data, information and knowledge for situation awareness 
through the use of robotic team members serving as sensory data, information and knowledge gatherers assisting human operators. 
The main application focus was disaster relief and humanitarian assistance." %}
{% include module.html image_path="/assets/images/kdb_sparql_pc_query.png" image_width=400 position="right" title="" description="The main components of auKsys were developed as part of the SymbiCloud module. This include the Knowledge Database (kDB) and the early prototype for the active query mechanism (scQL). The main achievement was the development of a system that handles distributed sensor data and semantic information across a set of heterogenous platforms. Allowing queries that combine both type of information, as shown on the picture on the right." %}

