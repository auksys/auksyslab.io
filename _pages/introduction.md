---
title: Introduction
excerpt: An introduction to the auKsys system.
permalink: /introduction/
---

auKsys (autonomous knowledge system) is a set of modular libraries that form a distributed knowledge system for multi agents, and in particular, auKsys is well suited for robotics and sensing applications.