---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/auksys-home-page-feature.jpg
  actions:
    - label: "<i class='fas fa-download'></i> Install now"
      url: "/documentation/installation/"
excerpt: >
  The distributed knowledge system for multi-agents, with an emphasis on robotics and sensing applications. Support acquiring, processing, storing and sharing. Handles sensor data to high-level semantic knowledge.<br />
feature_row:
  - image_path: /assets/images/auksys-features.png
    alt: "customizable"
    title: "Features"
    excerpt: "All the features provided by auksys."
    url: "/features/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/auksys-usecases.png
    alt: "use cases"
    title: "Use-cases"
    excerpt: "Example of use of auksys."
    url: "/use-cases/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/auksys-research.png
    alt: "research"
    title: "Research"
    excerpt: "Research that supported auksys!"
    url: "/research/"
    btn_class: "btn--primary"
    btn_label: "Learn more"      
---

{% include feature_row %}

News
----

<div class="feature__wrapper">
  {% for post in site.categories.news limit:3 %}
    <div class="feature__item">
      {% include archive-single.html %}
    </div>
  {% endfor %}
</div>

Blog
----

<div class="feature__wrapper">
  {% for post in site.categories.blog limit:3 %}
    <div class="feature__item">
      {% include archive-single.html %}
    </div>
  {% endfor %}
</div>
