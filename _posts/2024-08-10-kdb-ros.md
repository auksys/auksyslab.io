---
title:  "kDB / ROS"
date:   2024-08-10 12:38:32 +0200
categories: news
---

The ROS interface for kDB is now available as open source at [https://gitlab.com/auksys/ros_kdb](https://gitlab.com/auksys/ros_kdb). You can read more about the server and its capabilities in our documentation:

* [Installation instructions](/documentation/installation/).
* [kDB ROS Server 4.x](/documentation/4/ros/kdb_server/)
* [ROS Tutorials](/documentation/4/tutorials/ros/)
