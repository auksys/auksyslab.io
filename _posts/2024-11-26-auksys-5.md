---
title:  "auKsys/5 - Released"
date:   2024-11-26 08:57:11 +0200
categories: news
---

It is with great pleasure that we announce the release of `auksys/5`. The `stable` branch in our repositories correspond to that version of the distribution. `auksys/5` is compatible with [Ubuntu 24.04 and later](https://ubuntu.com/) and [ROS Jazzy](https://ros.org/).

You can check out our [installation instructions](/documentation/installation/).

Major changes
-------------

* Port to Qt6 and [cext](https://gitlab.com/cyloncore/cext/).
* Refactoring of `FocusNode` API, which uniformise access to `Datasets`, `Agents`... It was added to this release support for `Simple Geographic Features`, with a tool for extracting data from [OpenStreetMap](https://www.openstreetmap.org/)
* `knowX` is replaced with `knowBook` which is loosely inspired by [Jupyter Notebook](https://jupyter.org/). It currently allows to run SQL, SPARQL queries, extract data from [OpenStreetMap](https://www.openstreetmap.org/)

[![knowBook](/assets/images/20241126_knowBook.png)](/assets/images/20241126_knowBook.png)

* Improvements to the `kdb store` command, and early support for a MQTT server and better integration of the web interface.
* Switch to the new RDF Graph synchronisation protocol, with higher synchronisation efficiency under highly unreliable networking conditions.
* SMQuery was removed, and replaced with krQL.
* Docker images is a first class citizen for installing and distributing `auKsys`, images exists for the `stable` and development version, as well, as for older version (`legacy/4`). More information can be found in the [documentation](https://auksys.org/documentation/installation/docker/). We also introduced a tool [auksys](/documentation/5/programs/auksys/) for conveniently using the docker images.

We have a guide to help transition from [`auksys/4` to `auksys/5`](/documentation/5/from_4). The documentation is currently been updated, and in case of issues or questions, you can use our [support](https://gitlab.com/auksys/support).

Roadmap
-------

* The immediate roadmap includes updating the documentation.
* The general focus of the project is currently on improving the usability, through documentation, and better tooling.
* Tools that will receive special attentions are [knowBook](/documentation/5/programs/knowbook/) and the [mission and knowledge center](/documentation/5/programs/mkc/).
* Very soon llama support comes back in `pralin`.
* In the near future, we will include a module for properties graph.
