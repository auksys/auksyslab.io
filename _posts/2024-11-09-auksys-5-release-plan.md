---
title:  "auKsys 5 - Release Plan"
date:   2024-11-09 08:57:12 +0200
categories: news
---

`auKsys 5` is almost ready for release. The current plan is as follow:

* ~~`Monday 2024-11-14` commit freeze, only changes that fixes a failing tests are allowed~~.
* ~~`Thursday 2024-11-21` creation of `legacy` branches~~.
* ~~Planned for `Saturday 2024-11-23` release day, `stable` becomes `dev/5`~~.
