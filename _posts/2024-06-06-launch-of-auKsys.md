---
title:  "Launch of auKsys!"
date:   2024-06-06 12:38:32 +0200
categories: news
tags: auksys
---

The *auKsys* (*autonomous knowledge system*) project is been released as an open source project. This process will happen in stages, starting with *kDB* (the *knowledge database*).

 You can read more about the project and its capabilities in our documentation:

* [Installation instructions](/documentation/installation/).
* [auKsys 4.x](/documentation/4/)
* [Tutorials](/documentation/4/tutorials//)

