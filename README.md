auKsys website
==============

For testing the website locally, the first time you need to run the following to install dependencies:

```bash
make prepare
```

Then to run a local server for testing:

```bash
make test
```

The exact URL will be given in the terminal output, but is should be http://127.0.0.1:4000/.

After commiting and pushing on the `stable` branch, the website will be automatically built by gitalb. And it should appear at https://auksys.org/. You can check the build status at https://gitlab.com/auksys/auksys.gitlab.io/-/pipelines.
