Colors
======

This file list the color palette used by auKsys.

Primary colors
--------------

<style>
    .colorPalette {
        width: 70%;
        text-align: center;
    }
    .colorPalette th {
        background: grey;
        word-wrap: break-word;
        text-align: center;
    }
    .colorPalette tr:nth-child(1) td:nth-child(1) { background: #677D6A; }
    .colorPalette tr:nth-child(1) td:nth-child(2) { background: #a6b5a8; }
    .colorPalette tr:nth-child(1) td:nth-child(3) { background: #cfd8d1; }
    .colorPalette tr:nth-child(2) td:nth-child(1) { background: #7077A1; }
    .colorPalette tr:nth-child(2) td:nth-child(2) { background: #9da2be; }
    .colorPalette tr:nth-child(2) td:nth-child(3) { background: #caccdc; }
    .colorPalette tr:nth-child(3) td:nth-child(1) { background: #EB5B00; }
    .colorPalette tr:nth-child(3) td:nth-child(2) { background: #ff9b5b; }
    .colorPalette tr:nth-child(3) td:nth-child(3) { background: #ffcaa8; }
</style>

<div class="colorPalette">

| at L=53 | at L=68 | at L=83 | 
| --------| ------- | ------- |
| #7c937f | #a6b5a8 | #cfd8d1 |
| #6e75a0 | #9da2be | #caccdc |
| #ff6c0f | #ff9b5b | #ffcaa8 |

</div>
