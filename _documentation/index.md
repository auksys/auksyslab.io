---
title: auKsys Documentation
permalink: /documentation/
classes: wide
---

This is the general documentation of auKsys.

* Start with the [installation instructions](/documentation/installation/).
* And then go through our [tutorials](/documentation/4/tutorials/).
* [Distributions](/documentation/distributions/) contains information on the different version of auKsys.

The documentation for the different distributions:

* [stable/5](/documentation/5/)
* [legacy/4](/documentation/4/)

The documentation also contains the presentation of general concepts:

{% include documentation/concepts.md %}

