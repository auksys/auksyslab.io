---
title: auKsys Distributions
permalink: /documentation/distributions/
classes: wide
---

This page documents the auKsys distributions, and associated versions and GIT branches.

The table bellow is used to keep track of versions and branches used for the different auKsys distributions. There are currently three main GIT branches, `stable`, `dev/4` and `dev/5`. `dev/4` is the development branch for `stable`. While `dev/5`is the development branch for the future `stable`.

The following shows the targeted linux variant for the different auKsys versions:

|        | stable  | dev/5   | legacy/4       | dev/4          |
|--------|---------|---------|----------------|----------------|
| Ubuntu | 24.04   | 24.04   | 22.04          | 22.04          |
| Debian | Testing | Testing | Stable/Testing | Stable/Testing |

The following table shows the targeted ROS version for the different auKsys version:

|        | stable  | dev/5   | legacy/4       | dev/4          |
|--------|---------|---------|----------------|----------------|
| ROS    | Jazzy   | Jazzy   | Humble         | Humble         |

The following table shows the branches and version used for the different auKsys version:

|        | stable         | dev/5   | legacy/4       | dev/4   |
|--------|----------------|---------|----------------|---------|
| knowL  | stable (3.x)   | dev/3   | legacy/2       | dev/2   |
| kDB    | stable (5.x)   | dev/5   | legacy/4       | dev/4   |
| pralin | stable (3.x)   | dev/3   | legacy/2       | dev/2   |
