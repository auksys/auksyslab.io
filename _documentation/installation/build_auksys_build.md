---
title: Build auKsys using auksys-build
classes: wide
---

`auksys-build` is a tool for easilly install the dependencies and compile the auKsys librairies.
It requires `ruby` which can be installed with:

```bash
apt install ruby
```

The following can be used to get `auksys-build`, and run it:

```bash
git clone https://gitlab.com/auksys/auksys_build.git
cd auksys_build
./auḱsys-build [version]
```

Where `[version]` is one of the following:

* `stable` for Ubuntu 22.04 & Debian/Stable/Testing
* `dev/4` for the development version for Ubuntu 22.04 & Debian/Stable/Testing
* `dev/5` for the development version for Ubuntu 24.04 & Debian/Testing

More information regarding the different distributions can be found in the [distributions documentation](/documentation/distributions/).

The script will install the needed dependencies for `auKsys` and install them in `auksys_build/inst` directory. After running the above command, it is necesserary to source `auksys_build/config.bsah` (or `config.zsh`) to load auKsys in the shell environment.
