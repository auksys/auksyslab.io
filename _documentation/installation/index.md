---
title: auKsys Installation
permalink: /documentation/installation/
redirect_from:
  - /installation
---

*Warning:* we are in the process of open sourcing knowL/kDB and moving repositories to gitlab.com. This should be completed by mid-June.

This is how to install auKsys.

* [Build from source with auksys-build](/documentation/installation/build_auksys_build/) this is the recommended approach for users.
* [Build from source with cc-pkg](/documentation/installation/build_cc_pkg/) this is the recommended approach for developing and contributing to auKsys.
* [Build manually](/documentation/installation/build_manual/) instructions on how to build manually. This is *not* recommended.
* [Docker images](/documentation/installation/docker/) instructions on how to get Docker images.
* [ROS Server](/documentation/installation/ros/) instructions on how to build the ROS server.
