---
title: Build auKsys using cc-pkg
classes: wide
---

`cc-pkg` is a package management tool mainly recommended if you intend to do development on the auKsys libraries. For using the libraries, it is recommended to use [auksys-build](../build_auksys_build/).

Dependencies
------------

auKsys depends on the following libraries:

* PostgreSQL
* Qt (Quick, Positioning)
* eigen
* fmt

Optional dependencies:

* Ruby and Ruby rice
* Python
* pgpointcloud 

It is advised to install all the dependencies like in the [manual build instructions](../build_manual/).

Setup cc-pkg
------------

The first step is to install cc-pkg following [its install instructions](https://gitlab.com/cyloncore/cc-pkg/).

Then to fetch all the `auKsys` depedencies:

```bash
cc-pkg get --repo auksys https://gitlab.com/auksys/lrs-pkg-repository.git
```

Then install kDB with:

```bash
cc-pkg get Cartography kDB
cc-pkg build kDB
```

To keep the code updated:

```bash
cc-pkg update
cc-pkg build
```

More information on how to use `cc-pkg` is available in the command line documentation:

```bash
cc-pkg help
```
