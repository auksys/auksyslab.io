---
title: Docker images
classes: wide
---

The auKsys project provides up-to-date docker images at [docker hub](https://hub.docker.com/u/auksys), with ROS and non-ROS variant. We also provide with a tool for conveniently run docker images.


With `auksys` command (recommended)
===================================

Installation
------------

The `auksys` tool is implemented in Rust, most recent linux distribution provides it as a package, if not available check the [official installation page](https://www.rust-lang.org/tools/install).

On Ubuntu 24.04:

```bash
sudo apt install rustup
rustup default stable
```

After installation of rustup, the `auksys` command can be installed with:

```bash
cargo install auksys
```

Usage
-----

Documentation is provided as part of the command line interface:

```bash
auksys -h
```

### ROS Node

The `start` command is used to start a docker image. For instance to start a ROS node:

```bash
auksys start kdb/ros --name uav0
```

The name will be used as the ROS namespace. If no distribution is specified on the command line, `auksys` will attempt to guess the best distribution, depending on the `ROS_DISTRO` variable or will default to `stable`.

### Standalone kDB Store

A standalone store can be started with the following command:

```bash
start kdb/store --distribution dev/5 --web
```

It will start it with a web interface available on http://localhost:8888 . The flag `--mqtt` can also be used to start the store with a MQTT interface:

```bash
start kdb/store --distribution dev/5 --mqtt
```

**Note** the `kdb/store` option is available for `stable` and `dev/4` but with much more limited features.

As regular `docker`
===================

Images
------

Here are the following images depending on the version of the auKsys distributions:

|         | stable            | dev/5                   | legacy/4                   | dev/4                   |
|---------|-------------------|-------------------------|----------------------------|-------------------------|
| non-ros | auksys/auksys     | auksys/auksys:dev-5     | auksys/auksys:legacy-4     | auksys/auksys:dev-4     |
| ros     | auksys/ros_auksys | auksys/ros_auksys:dev-5 | auksys/ros_auksys:legacy-4 | auksys/ros_auksys:dev-4 |

Running the images require setting some environment variables, which are not fully documented. It is highly advise to use the `auksys` tool to generate command lines to access that information, for instance, the following command will display the `docker` line used to start the `ros_kdb` node:

```bash
auksys start kdb/ros --name uav0 --echo-cmd
```

In particular, the images use the following environment variable:

* `HOST_UID` and `HOST_GID` they should match the current user uid, so that mounted volume in the docker images can be read and write.
