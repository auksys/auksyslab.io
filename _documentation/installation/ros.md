---
title: Build ROS servers
classes: wide
---

kDB ROS Server
--------------

It is require to [build the auKsys libraries first](/installation/). Then in a ROS2 workspace, checkout the following two repositories:

If you have commit right access:

```
git clone git@gitlab.com:auksys/ros_kdb.git
git clone git@gitlab.com:auksys/ros_kdb_interfaces.git
```

Otherwise, for public access:

```
git clone https://gitlab.com/auksys2/ros_kdb.git
git clone https://gitlab.com/auksys2/ros_kdb_interfaces.git
```


Then build the workspace. You can then check [Getting started with the ROS server](/documentation/4/tutorials/ros/getting_started/) to see example of use of the server.

Computation ROS Server
----------------------

It is require to [build the auKsys libraries first](..). Then in a ROS2 workspace, checkout the following two repositories:

If you have commit right access:

```
git clone git@gitlab.com:auksys/ros_pralin.git
git clone git@gitlab.com:auksys/ros_pralin_interfaces.git
git clone https://gitlab.com/rosrb/ament_cmake_ruby.git
```

Otherwise, for public access:

```
git clone https://gitlab.com:auksys/ros_pralin.git
git clone https://gitlab.com:auksys/ros_pralin_interfaces.git
git clone https://gitlab.com/rosrb/ament_cmake_ruby.git
```


Then build the workspace. You can then check [Getting started with the ROS server](/documentation/4/tutorials/ros/getting_started/) to see example of use of the server.
