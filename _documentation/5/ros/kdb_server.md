---
title: Getting started with the kDB ROS Server
classes: wide
---

{% include documentation/5/banner.md %}

The kDB ROS Server is the main interface between a running ROS system and the knowledge database (kDB).

Introduction
------------

The ros_kdb package provide a server with the following functionnalities:

 - recording of topics
 - querying services (RDF Graph, Altitude...)
 - automatic synchronisation of RDF Graph between agents
 - data transfert between agents

Running
-------

The kDB ROS server is started with:

```bash
ros2 run ros_kdb kdb_server
```

This will give access to all the functionnalities of the server, the database will be stored in `$HOME/.ros_kdb/default`. If a namespace is provided when starting the robot, with:

```bash
ros2 run ros_kdb kdb_server __ns:=/robot_ns
```

In this case, the database is stored in `$HOME/.ros_kdb/robot_ns`.


Services/Topics
---------------

### Base Knowledge

Many of the functionnalities of `ros_kdb` requires a base knoweldge, which is data stored in a directories, containing DEM, RDF Graph... Base knowledge can also be defined as python or ruby scripts.

Base knowledge directories can be added wit a krQL query, for instance:

```bash
ros2 service call /robot_ns/kdb_server/krql_query ros_kdb_interfaces/srv/QueryDatabase "queries: [{query: 'REGISTER BASE KNOWLEDGE directory: \"/path/to/directory\"'}]"
``` 

### Topic Recording


### SQL Query

### SPARQL Query


### Altitude Query

The `kdb_server/get_altitude` service allows to access the altitude given the coordinate. This service is only working if you have loaded a digital elevation map of the area, for instance, using the base knowledge functionnality.

```bash
ros2 service call /robot_ns/kdb_server/get_altitude ros_kdb_interfaces/srv/GetAltitude "{latitude: 58.494844, longitude: 15.102244, srid: 4326}"
```


### Graph Synchronisation


### Dataset Transfert


Tutorials
---------

The following topics are covered for ROS tutorials:

* {% include documentation/5/tutorials/ros/getting_started.md %}
* {% include documentation/5/tutorials/ros/querying.md %}
* {% include documentation/5/tutorials/ros/graph_sync.md %}
