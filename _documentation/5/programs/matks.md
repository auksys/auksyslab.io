---
title: matks
classes: wide
---

{% include documentation/5/banner.md %}

**WARNING this page documents an upcoming component, in early phases of development, subject to change and not ready for production.**

`matks` (**m**ulti-**a**gent **t**ask and **k**nowledge **s**imulator) is a simulator for agents that simulate *task* and *knowledge*. It is intended to simulate large numbers of agents.

Gitlab repository is at [https://gitlab.com/auksys/matks/](https://gitlab.com/auksys/matks/).

Overview
--------

The figure below provides an overview of an agent and the relation to different type of simulator:

[![image overview of matks](/assets/images/matks_overview.png)](/assets/images/matks_overview.png)

A typical agent is composed of many different layers, from low-level interface to hardware through `controlers` and `drivers` to high-level decision layer with `task planners` and `deciders`. The [Gazebo](https://gazebosim.org/home) simulator is an example of low-level simulator, which simulates agents at a driver level. The output of the `Gazebo` simulator is simulated sensor data and the input is control signals for the actuators. This type of simulation is computationally intensive and very useful to develop the low-level aspects of an agent. `matks` is a middle-level simulator, it simulates the execution of a task and the generation of high-level knowledge. The main purpose of `matks` is to enable the simulation of very large number of agents to be able to test and evaluate how high level algorithms scale with a large number of agents.

Interfacing with matks
----------------------

`matks` communicate through the following MQTT topics:

* `matks/positions` the positions (in longitude/latitude) of all the agents.
* `matks/image` (**experimental**) send simulated images. The images are cropped from an orthographic images.

The delegation system of `agents-tk` also uses the following topics:

{% include documentation/5/libraries/agents_tk/delegation_mqtt_topics.md %}
