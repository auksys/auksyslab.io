---
title: auksys
classes: wide
---

{% include documentation/5/banner.md %}

`auksys` is a tool for running `kDB`, `pralin`, `scql` using the *official* docker images. The source code is available in [gitlab](https://gitlab.com/auksys/auksys).

Quick Start
-----------

Provided Rust is installed in the system (either via package or the [official install guide](https://www.rust-lang.org/tools/install)):

```bash
cargo install auksys
```

Then running the following will start a kDB store with a web interface:

```bash
auksys start kdb/store --web
```

The `auksys` tool includes online help with the list of different commands and usage:

```bash
auksys --help
```

Further reading
---------------

* Use of `auKsys` [docker](/documentation/installation/docker/) images.
* The [tutorials](/documentation/5/tutorials/) contain example of use of `auksys`.
