---
title: mkc
classes: wide
---

{% include documentation/5/banner.md %}

**WARNING this page documents an upcoming component, in early phases of development, subject to change and not ready for production.**

`mkc` (**m**ission and **k**nowledge **c**enter) is a set of GUI applications intended to interface with a multi-agent system that can execute *missions* and gather *knowledge*.

Gitlab repository is at [https://gitlab.com/auksys/mkc_clients/](https://gitlab.com/auksys/mkc_clients/) and [https://gitlab.com/auksys/mkc_server/](https://gitlab.com/auksys/mkc_server/).

Overview
--------

The figure below provides an overview of `mkc` and the releation between the clients and live systems through an *application server* (`mkc_server`).

[![image overview of mkc](/assets/images/mkc_architecture.png)](/assets/images/mkc_architecture.png)

Clients are connected to an `mkc_server` which acts as the gateway to a live system. The live system can be simulated, or actual platforms. `mkc_server` is agnostics to the middle-ware, and it currently supports by default either MQTT or ROS.

The `mkc_server` communicates with the clients using an HTTP server, via a REST API. This offers flexibility for the development toolkit used by the client. However, the official clients are developed using the [Qt Framework](https://www.qt.io/).

Clients
-------

### Desktop

The desktop client is currently our primary targets, it allows to display infomation about the system. For instance, it can show agents and datasets for the current operation environment:

[![image overview of matks](/assets/images/mkc_desktop_agents.png){: style="width:48%" }](/assets/images/mkc_desktop_agents.png)
[![image overview of matks](/assets/images/mkc_desktop_datasets.png){: style="width:48%" }](/assets/images/mkc_desktop_datasets.png)

The client can also be used to start and delegate missions.
