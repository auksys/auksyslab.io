---
title: knowBook
classes: wide
---

{% include documentation/5/banner.md %}

`knowBook` is a tool for accessing the knowledge stored in `auKsys`, mainly targeting developers.

[![knowBook](/assets/images/20241126_knowBook.png)](/assets/images/20241126_knowBook.png)

knowBook Tutorials
-------------------

{% include documentation/5/tutorials/knowbook.md %}
