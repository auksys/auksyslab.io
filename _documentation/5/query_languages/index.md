---
title: auKsys Query Languages Documentation
permalink: /documentation/5/query_languages/
classes: wide
---

{% include documentation/5/banner.md %}

{% include documentation/5/query_languages.md %}
