---
title: auKsys Documentation
permalink: /documentation/5/from_4
classes: wide
---

This page covers the changes from version 4, and should help migrating to *auKsys/5*.

General
-------

* *SMQ* was dropped and is replaced with *krQL, we refer you to the [krQL documentation](/documentation/5/query_languages/krql/) to help you adapt. Syntaxically, *SMQ* and *krQL* are similar, the main difference is that *krQL* is based on YAML instead of a custom key/value syntax.
* A number of ontologies have been cleaning, and we refer to https://askco.re/ for up-to-date information about ontologies.
* Focus nodes API have been cleaned, and standardized. This affect *datasets*, *agents* and *salient regions*. We refer to [salient regions](/documentation/5/tutorials/data/salient_regions/) for up-to-date instructions on how to create salient regions, and how to replace the *SalientRegionBuilder*.

Library
-------

* *TriplesStore* was renamed to *TripleStore*

ROS
---

* The triples store storage has been moved to *~/.local/share/auKsys/5/kdb/stores/ros/${namespace}* (from *~/ros_kdb/stores/${namespace}*).
