---
title: kDB / Web Server
classes: wide
---

{% include documentation/5/banner.md %}

The `kDBWebServer` is an extension that enables a web server which can be used to interract with a store.
The webserver deliver both an HTML user interface (accessible by default as `http://localhost:8888/`) and an API.

API
===

* `/sparql.html` is a [SPARQL](/documentation/5/query_languages/sparql/) end-point compliant with [SPARQL 1.1 Protocol](https://www.w3.org/TR/sparql11-protocol/), it supports the following request parameters:
  * `query` (exactly 1) the query string
  * `default-graph-uri` (0 or 1, available in kDB >= 5.1) the URI of the default graph for the query
  * `Accept` (0 or more) format for the result of the query. `kDBWebServer` currently support the following format: `text/html`, `application/sparql-results+json` and `application/sparql-results+xml`.

* `/krql.html` is a used to execute [krQL](/documentation/5/query_languages/krql/) queries. It supports the `query` and `Accept` parameters.

Further Reading
===============

* {% include documentation/5/tutorials/web_server.md %}
