---
title: agent-tk
classes: wide
---

{% include documentation/5/banner.md %}

**WARNING this page documents an upcoming component, in early phases of development, subject to change and not ready for production.**

`agent-tk` (agent-toolkit/task-knowledge) is a library to implement light weight task and knowledge base agent. It provides abstract interfaces to integrate knowledge and decision process. The decision process in `agent-tk` is based on the delegation system proposed in {%cite DOHERTY-DELEGATION-2013 %}.

`agent-tk` is currently developed for providing support to the [matks simulator](/documentation/5/programs/matks/), and it is planned to extend it for support IOT systems in the future.

Gitlab repository is at [https://gitlab.com/auksys/agent_tk/](https://gitlab.com/auksys/agent_tk/).

Overview
--------

The figure below provides an overview of an agent as defined in `agent-tk`:

[![image overview of agent-tk](/assets/images/agent_tk_architecture.png)](/assets/images/agent_tk_architecture.png)

On a high level, an agent is composed of three group of systems:

* `definitions` contains static metadata describing the agents, in particular, its `capabilities`.
* `resources` contains dynamic information about the agent and its environment.
  * `knowledge base` is used to store information about the past and the current state of the world. `agent-tk` can use a simple in-memory store or connect to an instance of [kDB](/documentation/5/libraries/kdb/).
  * `states` contains a snapshot of the current state of the robot, such as position/velocity. The `platform` is sending update to the `states`.
  * `resources` contains the resources of the agent: sensors, actuators... This module allow to lock the resources to avoid concurrent use.
* `components` contains active module involves in decision or in execution of task.
  * `execution` contains a queue of tasks to be executed by the agent. It interracts with the low-level platform.
  * `decision` takes decision on the feasibility and cost of executing a given task. It use a `simulator` to estimate the cost of a task.
  * `delegation` module is used to distribute task among agent. This module can receive `goal/tst` to execute from the agent or it received call for proposal (CFP) from other agents. Upon receiving a CFP, the `delegation` module ask the `decision` module for a cost and feasability to execute a given CFP.

Interfacing with agent-tk (via MQTT)
------------------------------------

The delegation system of `agents-tk` also uses the following topics:

{% include documentation/5/libraries/agents_tk/delegation_mqtt_topics.md %}

The default way to send tasks to an `agent-tk` agent is using the delegation protocol. However, in case it is not desirable to interract directly with protocol, `agent-tk` provides a delegation server called `agent-tk-delegation-server` which uses a MQTT-based service call to start a delegation, using the following topic:

* `delegation_server/delegate` send a request to delegate a Goal or a TST. This rely on the `MQTT v5 Request-Response Pattern`, and requires that the correlation ID and response topics are set.

References
----------

{% bibliography --cited_in_order  %}
