---
title: pralin
classes: wide
permalink: /documentation/5/libraries/pralin/
---

{% include documentation/5/banner.md %}

pralin (**pr**ocessing **al**gorithms **in**terfaces) is a C++ library that provides interfaces between algorithms used for processing data. *pralin* currently support tensors, images and point clouds. *pralin* provides interface to some of the most common libraries, from computer vision, natural language processing, point clouds...

Modules
=======

This is the list of currently integrated libraries:

* [curl](https://curl.se/): curl is a library for transferring data using various network protocols. The pralin module allows to access remote API.
* [darknet](https://pjreddie.com/darknet/): Darknet is an open source neural network framework written in C and CUDA.
* [euclid](https://gitlab.com/cyloncore/euclid/): euclid is a C++ template library for interfacing with computational geometry library.
* [fasttext](https://fasttext.cc/): library for efficient text classification and representation learning.
* [OpenCV](https://opencv.org/): OpenCV (Open Source Computer Vision Library) is a library of programming functions mainly for real-time computer vision.
* [PCL](https://pointclouds.org/): The Point Cloud Library (PCL) is a standalone, large scale, open project for 2D/3D image and point cloud processing.
* [proj](https://proj.org/): PROJ is a generic coordinate transformation software that transforms geospatial coordinates from one coordinate reference system (CRS) to another.
* [pytorch](https://pytorch.org/): PyTorch is a machine learning framework used for applications such as computer vision and natural language processing.
* [sentencepiece](https://github.com/google/sentencepiece): SentencePiece is an unsupervised text tokenizer and detokenizer mainly for Neural Network-based text generation systems where the vocabulary size is predetermined prior to the neural model training.
* [word2vec](https://github.com/maxoodf/word2vec): is a Distributed Representations of Words (word2vec) library.
* [ZBar](https://zbar.sourceforge.net/): ZBar is an open source software suite for reading bar codes from various sources, such as video streams, image files and raw intensity sensors. It supports many popular symbologies (types of bar codes) including EAN-13/UPC-A, UPC-E, EAN-8, Code 128, Code 39, Interleaved 2 of 5 and QR Code.

Compose
=======

`pralin/compose` is a library for running a composition of algorithms, described as YAML documents, which is documented in [pralin/compose](compose/).

Tutorials
=========

{% include documentation/5/tutorials/pralin.md %}
