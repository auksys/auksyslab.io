---
title: Advanced Querying
classes: wide
---

{% include documentation/5/banner.md %}

The [Getting Started Tutorial](/documentation/5/tutorials/getting_started/) covered the basic of querying with kDB. This tutorial covers advanced topics, such as the use of bindings and of the RDFEnvironment.


Bindings
--------

Bindings allow to reference values used in queries by a keyword which is then substitude by the value during the query execution.
Lets consider the following SQL query, which does not use bindings:

```sql
SELECT * FROM triples_stores WHERE name='http://askco.re/graphs#default';
```

This query can be rewritten using the binding `:name` as:

```sql
SELECT * FROM triples_stores WHERE name=:name;
```

And then before executing the query, it is necesserary to specify the value for `:name`.

The same principle can be applied to SPARQL query, execpt the name of the binding must start with `%`, for instance:

```sparql
SELECT ?y ?z WHERE { %subject ?y ?z . };
```

Bindings API
------------

Bindings are added to the query using the `addBinding` functions of the `knowDBC::Query`, as demonstrated bellow:

<div class="tab-grey-background">

{% tabs query %}

{% tab query C++ %}
```c++
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

// Assuming a 'connection' was created.

// The following creates an SQL query, listing the content of the triples_stores table.
knowDBC::Query query = connection.createSQLQuery("SELECT * FROM triples_stores WHERE name=:name");

// Bind the value "http://askco.re/graphs#default" to the binding ":name", note that ':'
// is required.
query.bindValue(":name", "http://askco.re/graphs#default");

// The following execute either the SQL or SPARQL query
knowDBC::Result result = query.execute();
```
{% endtab %}

{% tab query Python %}
```python
# Assuming a 'connection' was created.

# The following creates an SQL query, listing the content of the triples_stores table.
query = connection.createSQLQuery("SELECT * FROM triples_stores WHERE name=:name")

# Bind the value "http://askco.re/graphs#default" to the binding ":name", note that ':'
# is required.
query.bindValue(":name", "http://askco.re/graphs#default")
```
{% endtab %}

{% tab query Ruby %}
```ruby
# Assuming a 'connection' was created.

# The following creates an SQL query, listing the content of the triples_stores table.
query = connection.createSQLQuery "SELECT * FROM triples_stores WHERE name=:name"

# Bind the value "http://askco.re/graphs#default" to the binding ":name", note that ':'
# is required.
query.bindValue ":name", "http://askco.re/graphs#default"
```
{% endtab %}

{% endtabs %}

</div>


RDF Environment
---------------

The RDF Environment is used to define the context in which a SPARQL query is executed.
It contains the following information:

* `base url` the base URL used for all relative links in the query (can be overwritten with the `@base` keyword)
* `default dataset` the main dataset used for querying (can be overwritten with the `FROM` keyword)
* `named datasets` a list of datasets that can be referenced with their names in SPARQL query (can be overwritten with the `FROM NAMED` keywords).
* `services` a list of RDF Services that can be used in the query (usually for executing remote queries).


<div class="tab-grey-background">

{% tabs rdf_environment %}

{% tab rdf_environment C++ %}
```c++
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>
#include <kDB/Repository/RDFEnvironment.h>

// Assuming a 'connection' was created.

// The following creates an SPARQL Query and set the base, default dataset and add a named dataset
knowDBC::Query query = connection.createSPARQLQuery(
  kDB::Repository::RDFEnvironment()
    .setBase("file:///home/kdb/query.rq#")
    .setDefaultDataset(connection.graphsManager()->getTripleStore("http://askco.re/graphs#default"_kCu))
    .addNamedDataset(connection.graphsManager()->getTripleStore("http://askco.re/graphs#info"_kCu))
    ...,
  "...");
```
{% endtab %}

{% tab rdf_environment Python %}
```python
from knowCore import Uri

# Assuming a 'connection' was created.

# The following creates an SPARQL Query and set the base, default dataset and add a named dataset
query = connection.createSPARQLQuery(
  kDB.Repository.RDFEnvironment()
    .setBase(Uri("file:///home/kdb/query.rq#"))
    .setDefaultDataset(connection.graphsManager().getTriplesStore(Uri("http://askco.re/graphs#default")))
    .addNamedDataset(connection.graphsManager().getTriplesStore(Uri("http://askco.re/graphs#info")))
    ...,
  "...");
```
{% endtab %}

{% tab rdf_environment Ruby %}
```ruby
# Assuming a 'connection' was created.

# The following creates an SPARQL Query and set the base, default dataset and add a named dataset
query = connection.createSPARQLQuery(
  kDB.Repository.RDFEnvironment()
    .setBase(kCu("file:///home/kdb/query.rq#"))
    .setDefaultDataset(connection.graphsManager().getTriplesStore(kCu("http://askco.re/graphs#default")))
    .addNamedDataset(connection.graphsManager().getTriplesStore(kCu("http://askco.re/graphs#info")))
    ...,
  "...");
```
{% endtab %}

{% endtabs %}

RDF Union
---------

It is also possible to query against the union of two RDF Datasets.

{% tabs rdf_union %}

{% tab rdf_union C++ %}
```c++
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>
#include <knowDBC/Result.h>
#include <kDB/Repository/RDFEnvironment.h>
#include <kDB/Repository/DatasetsUnion.h>

// Assuming a 'connection' was created.

// The following creates a dataset union
kDB::Repository::DatasetsUnion dsu;
dsu.add(connection.graphsManager()->getTripleStore("http://askco.re/graphs#default"_kCu));
dsu.add(connection.graphsManager()->getTripleStore("http://askco.re/graphs#info"_kCu));

// The following creates an SPARQL Query against that union
knowDBC::Query query = connection.createSPARQLQuery(
  kDB::Repository::RDFEnvironment(dsu),
  "...");
```
{% endtab %}

{% tab rdf_union Python %}
```python
from knowCore import Uri

# Assuming a 'connection' was created.

# The following creates a dataset union
dsu = kDB.Repository.DatasetsUnion()
dsu.add(connection.graphsManager().getTriplesStore(Uri("http://askco.re/graphs#default")))
dsu.add(connection.graphsManager().getTriplesStore(Uri("http://askco.re/graphs#info")))

# The following creates an SPARQL Query against that union
query = connection.createSPARQLQuery(
  kDB.Repository.RDFEnvironment(dsu),
  "...");
```
{% endtab %}

{% tab rdf_union Ruby %}
```ruby
# Assuming a 'connection' was created.

# The following creates a dataset union
dsu = KDB::Repository::DatasetsUnion.new
dsu.add connection.graphsManager.getTriplesStore kCu("http://askco.re/graphs#default")
dsu.add connection.graphsManager.getTriplesStore kCu("http://askco.re/graphs#info")

# The following creates an SPARQL Query against that union
query = connection.createSPARQLQuery
  kDB.Repository.RDFEnvironment.new(dsu),
  "..."
```
{% endtab %}

{% endtabs %}


</div>

