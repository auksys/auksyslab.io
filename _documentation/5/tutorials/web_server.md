---
title: Web Server
classes: wide
---

{% include documentation/5/banner.md %}

This tutorial presents how to enable and access the kDB Web Server.

Enable the Web Server
=====================

The web server can only be enabled using C++ or the command line.

<div class="tab-grey-background">

{% tabs enable_web_server %}

{% tab enable_web_server c++ %}

```c++
#include <kDBWebServer/EndPoint.h>

kDBWebServer::EndPoint ep(kDBHostName, kDBPort);
```

{% endtab %}

{% tab enable_web_server Cmd %}

When starting with `auksys` or `kdb` tools, it is possible to use the `--web` option:

```bash
auksys start kdb/store --web
kdb store --web
```

The `web-server` command of `kdb` tool can also be used to start a web server for an already running store:

```bash
kdb web-server
```

This can be used to start a web server for a store started from python, ruby or as a ROS server.
{% endtab %}

{% endtabs %}

</div>


Call the Web Server API
=======================

The web server API documentation is available in the [kDBWebServer documentation](/documentation/5/libraries/kdb/web_server/). In this tutorial we present simple example to use that API to execute a SPARQL query.


<div class="tab-grey-background">

{% tabs call_api %}

{% tab call_api Python %}

Using python `requests`, which can be installed with:

```bash
python -m pip install requests
```

The following command can be used to execute the `SELECT ?x ?y ?z WHERE {?x ?y ?z .}` query on `http://askco.re/graph#info` and return a JSON file.

```python
import requests

# Prepare the parameters for the query
request = {"query": "SELECT ?x ?y ?z WHERE {?x ?y ?z .}", "Accept": "application/sparql-results+json", "default-graph-uri": "http://askco.re/graph#info"}

# Execute the request
response = requests.post("http://localhost:8888/sparql.html", data=request)

# Parse the resulting json data
json = response.json()
```

{% endtab %}

{% tab call_api Ruby %}

```ruby
require 'net/http'
require 'json'

# Execute the request
uri = URI('http://localhost:8888/sparql.html')
res = Net::HTTP.post_form(uri, 'query' => 'SELECT ?x ?y ?z WHERE {?x ?y ?z .}', 'Accept' => 'application/sparql-results+json', 'default-graph-uri': 'http://askco.re/graph#info')

# Parse the resulting json data
json = JSON.parse(res.body)
```

{% endtab %}

{% tab call_api Cmd %}

The general template for calling the end point from the command line follow:

```bash
curl "http://localhost:8888/sparql.html?query=<insert encoded query>&Accept=<insert encoded format>&default-graph-uri=<insert default graph>"
```

The following command can be used to execute the `SELECT ?x ?y ?z WHERE {?x ?y ?z .}` query on `http://askco.re/graph#info` and return a JSON file.

```bash
curl "http://localhost:8888/sparql.html?query=SELECT%20%3Fx%20%3Fy%20%3Fz%20WHERE%20%7B%3Fx%20%3Fy%20%3Fz%20.%7D&Accept=application%2Fsparql-results%2Bjson&default-graph-uri=http%3A%2F%2Faskco.re%2Fgraph%23info"
```

Alternatively you can let curl encode the URL as POST, using:

```bash
curl --data-urlencode "query=SELECT ?x ?y ?z WHERE {?x ?y ?z .}" --data-urlencode "Accept=application/sparql-results+json" --data-urlencode "default-graph-uri=http://askco.re/graph#info" http://localhost:8888/sparql.html
```

{% endtab %}

{% endtabs %}


