---
title: OSM Extraction
classes: wide
---

After starting `knowBook` and connecting to a running kDB server, the `OSM Extraction` tool can be used to extract OSM Data into an RDF document using `Simple Geometric Features`.

After adding the tool, it looks like the following image:

[![knowBook OSM Extraction](/assets/images/20250226_knowBook_osm_extraction.png)](/assets/images/20250226_knowBook_osm_extraction.png)

* When `pan` is selected, you can use the mouse to drag the map to the location of interest and zoom using the mouse wheel.
* After clicking on `select`, you can use the mouse to define the area where to extract the information.
* Write in the `Destination` field the name of the RDF Graph used to store the extracted data.
* Finally, click on the compute icon to start the extraction.

**Note**

* Avoid selecting large areas, to avoid overloading the open street map servers.
* Respect the OpenStreetMap [license](https://www.openstreetmap.org/copyright).
* `knowBook` is still in `alpha` stage, and will freeze during the extraction process, which can take a long time. So be *patient*.
`