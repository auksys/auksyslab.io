---
title: Getting Started With ROS
classes: wide
---

{% include documentation/5/banner.md %}

This tutorial covers the basics of running a kDB Store as a ROS Node.
The full documentation of the ROS Node is available [here](/documentation/5/ros/kdb_server/).
The following command can be used to start a ROS node with a kDB Store:

Start server
------------

```bash
# The following will create a PostgreSQL database in '$HOME/.ros_kdb/stores/default' using
# the default port 10141:
ros2 run ros_kdb kdb_server
```

When running multiple instance on the same computer, it is required to use namespaces and specify the port for the database:

```bash
# The following will create a PostgreSQL database in '$HOME/.ros_kdb/stores/default' using
# the default port 10142:
ros2 run ros_kdb kdb_server --ros-args -r __ns:=/ -p db_port:=10142
```

Query database
--------------

Then it is possible to query the database using the following service calls:

```bash
# The following executes an SQL query, listing the content of the triples_stores table.
ros2 service call /kdb_server/sql_query ros_kdb_interfaces/srv/QueryDatabase 'queries: [ { query: "SELECT * FROM triples_stores" } ]'

# The following creates an SPARQL query, listing the content of an RDF Graph.
ros2 service call /kdb_server/sparql_query ros_kdb_interfaces/srv/QueryDatabase 'queries: [ { graphnames: ["http://askco.re/graphs#info"], query: "SELECT * WHERE { ?x ?y ?z. }" } ]'
```

Loading RDF Data
----------------

The following commands can be used to load RDF data in a triple store.
In this example it will load the data in the `http://askco.re/examples#map` triple store.

```bash
# Replace the ... with the ttl data:
ros2 service call /kdb_server/insert_triples ros_kdb_interfaces/srv/InsertTriples "
graphname: 'http://askco.re/examples#map'
format: 'ttl'
content: '...'"

# If the deta is in the file `data.ttl` it can be loaded with:
ros2 service call /kdb_server/insert_triples ros_kdb_interfaces/srv/InsertTriples "
graphname: 'http://askco.re/examples#map'
format: 'ttl'
content: '`cat data.ttl`'"
```

For more information on triple store, check the [triple stores tutorial](/documentation/5/tutorials/triple_stores/).

Next
----

The following tutorials expand on various aspect of the kDB ROS Node:

* {% include documentation/5/tutorials/ros/querying.md %}
* {% include documentation/5/tutorials/ros/graph_sync.md %}
* {% include documentation/5/tutorials/ros/record_sensor_data.md %}
* {% include documentation/5/tutorials/ros/dataset_transfer.md %}
