---
title: ROS Computation Server
classes: wide
---

{% include documentation/5/banner.md %}

The computation server is part of the `ros_pralin` package, and it allows to run [pralin composition](/documentation/5/tutorials/pralin/compose/introduction/) as a service.

It is started with:

```bash
ros2 run ros_pralin computation_server
```

New computation can be started with the `start_processing` (`ros_pralin_interfaces/srv/StartProcessing`) service call. However, it can be difficult to pass computation as argument to `ros2 service call`, it is therefore recommended to use `run_computation` when starting computation from the command line, for instance, the following will run the computation defined in `addition.yaml` (you can use the computation from [pralin/compose introduction](/documentation/5/tutorials/pralin/compose/introduction/) as an example):

```bash
ros2 run ros_pralin run_computation --namespace addition addition.yaml
```

The `--namespace` is used to specify that the topic created for the computation server should be in the `/addition` namespace. To select the ROS namespace for the computation server, this is done with `--ros-args -r __ns:=/server_namespace`. 

You can also run the following and it will read the computation from the terminal input:

```bash
ros2 run ros_pralin run_computation --stdin --namespace addition
```

When running the computation from the introduction tutorial, this will create three topics: `/addition/a`, `/addition/b` and `/addition/c`:

```bash
# In a terminal
ros2 topic echo /addition/output_c

# In an other terminal
ros2 topic pub --once /addition/input_b std_msgs/msg/Float64 "data: 2.0"
ros2 topic pub --once /addition/input_a std_msgs/msg/Float64 "data: 1.0"
```

The calculation will be completed once the computation server has receive one message on each topic.
To keep the calculation running, we can pass the `--repeat` option:

```bash
ros2 run ros_pralin run_computation --repeat --namespace addition addition.yaml
```

Computation that are repeating needs to be stopped with a service call to `stop_processing`:

```bash
ros2 service call /stop_processing ros_pralin_interfaces/srv/ControlProcessing "uuid: 'COMPUTATION UUID'"
```

You need to replace `'COMPUTATION UUID'` with the UUID show when executing `run_computation`.

Integration with kDB
--------------------

Assuming kDB and the computation server are started in the same ROS namespace, it is possible to create a connection to a kDB server using the following pralin operation:

```yaml
- ros_kdb/create_connection_handle:
    id: connection
```

For instance, the following composition can be used to extract all images from a dataset:

```yaml
compose:
  variables:
    counter: &counter [0, 'add[0]']
  parameters:
    source_dataset_uri: !required null
  process:
    - ros_kdb/create_connection_handle:
        id: connection
    - kdb/sensing/query_image_dataset:
        id: qid
        inputs: ["connection[0]"]
        parameters:
          query: !param source_dataset_uri
    - for_each:
        id: iterate_image
        iterator: qid[0]
        process:
          - pralin/values/string/format:
              id: filename
              inputs: [ [*counter] ]
              parameters:
                format: "{}.jpg"
          - opencv/imgcodecs/imwrite:
              id: imwrite
              inputs: [ "filename[0]", "iterate_image[0]"]
          - pralin/arithmetic/addition:
              id: add
              inputs: [ *counter, 1]
```

Assuming a dataset with images was loaded in the database, through [image recording](/documentation/5/tutorials/ros/record_sensor_data/) or by importing, like it is done at the beginning of the [dataset transfer tutorial](/documentation/5/tutorials/ros/dataset_transfer/).
The following show how to pass the parameters as argument to `run_computation`:

```bash
ros2 run ros_pralin run_computation --parameters "{ source_dataset_uri: 'http://askco.re/examples#images_granso' }" --stdin
```

For more detail on how to process image datasets, check the [process images datasets tutorial](/documentation/5/tutorials/pralin/compose/process_images_datasets/).

Next
----

* {% include documentation/5/tutorials/pralin/compose/introduction.md %}
