---
title: Base Knowledge
classes: wide
---

{% include documentation/5/banner.md %}

Most common usage of `kDB` is for handling dynamic information in an autonomous system.
However, such systems might be using some static information, such as geographic information, ontologies, automated planning definitions...
Such information is handled by the `kDBBaseKnowledge` module.
The module handles loading of the information in the database, it also monitor changes in the file system to update the content of the database, in case the base knowledge is updated.

Enabling kDBBaseKnowledge
-------------------------

`kDBBaseKnowledge` is provided as an extension, and need to be enabled in the store before it is used.
This needs to be done only once after creating the store.
Once an extension has been enabled, it is automatically available to all connections (current and future).

<div class="tab-grey-background">

{% tabs enable_kDBBaseKnowledge %}

{% tab enable_kDBBaseKnowledge C++ %}
{% raw %}
```c++
connection.enableExtension("kDBBaseKnowledge");
```
{% endraw %}
{% endtab %}

{% tab enable_kDBBaseKnowledge Python %}

```python
connection.enableExtension("kDBBaseKnowledge")
```

{% endtab %}

{% tab enable_kDBBaseKnowledge Ruby %}

```ruby
connection.enableExtension("kDBBaseKnowledge")
```

{% endtab %}

{% endtabs %}

</div>

Loading base knowledge
----------------------

<div class="tab-grey-background">

{% tabs add_to_base_knowledge %}

{% tab add_to_base_knowledge C++ %}
{% raw %}
```c++
kDBBaseKnowledge::Manager* m = c1.extensionObject<kDBBaseKnowledge::Manager>();

// To add a directory
m->addDirectory(...);

// To add a file
m->addFile(...);

```
{% endraw %}
{% endtab %}

{% tab add_to_base_knowledge Python %}

`kDBBaseKnowledge` is not directly available in Python, but can be access via the `krQL` API:

```python
connection.krQueryEngine().execute("...")
```

See queries in the `krQL` tab.

{% endtab %}

{% tab add_to_base_knowledge Ruby %}

`kDBBaseKnowledge` is not directly available in Ruby, but can be access via the `krQL` API:

```ruby
connection.krQueryEngine.execute "..."
```

See queries in the `krQL` tab.

{% endtab %}

{% tab add_to_base_knowledge krQL %}

The following query can be used to add all the files in a directory:

```yaml
register base knowledge:
  directory: '...'
```

The following query can be used to add a file:

```yaml
register base knowledge:
  file: '...'
```

{% endtab %}

{% endtabs %}

</div>

Supported Data
--------------

The following type of data can be loaded using the base knowledge module:

* Orthoimages
* DEM
* RDF Data can be inserted using a SPARQL Update query
* ttl files, via the execution of SPARQL Update queries
* kdQL documents, via the execution of kdQL queries

### TTL Files

To load a TTL file with the base knowledge, you can use the following SPARQL Update query:

```sparql
DROP GRAPH <http://example.org/my_rdf_data>;
LOAD <my_rdf_data.ttl> INTO GRAPH <http://example.org/my_rdf_data> 
```

This query will load the file `my_rdf_data.ttl` in the `http://example.org/my_rdf_data` graph.
The query needs to be saved in a file with the extension `.rq`, for instance, `load_my_rdf_data.rq`.

### kdQL Documents

`kdQL` documents are created using a `kdQL` query, for instance:

```yaml
%TAG ! tag:askco.re/kdql,2023:
- drop:
    - my_data
- create:
    into: my_data
    documents:
      - name: some name
        src: !include some_large_outside_file
        tags: [ 'a tag', 'and another' ]
      - name: some other name
        src: "I only have small amount of data."
        tags: [ 'I also have a tag' ]
```

This query create a collection of documents `my_data` with two documents. The tag `!include` can be used to load data saved in a seperate file. The query needs to be saved in a file with the extension `.kdql`, for instance, `load_my_data.kdql`.

