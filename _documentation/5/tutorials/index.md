---
title: Tutorials
permalink: /documentation/5/tutorials/
---

{% include documentation/5/banner.md %}

Basic Tutorials
---------------

* {% include documentation/5/tutorials/getting_started.md %}
* {% include documentation/5/tutorials/triple_stores.md %}
* {% include documentation/5/tutorials/advanced_querying.md %}
* {% include documentation/5/tutorials/base_knowledge.md %}
* {% include documentation/5/tutorials/web_server.md %}

Data Tutorials
--------------

{% include documentation/5/tutorials/data.md %}

ROS Tutorials
-------------

{% include documentation/5/tutorials/ros.md %}

pralin/compose Tutorials
------------------------

{% include documentation/5/tutorials/pralin/compose.md %}

knowBook Tutorials
-------------------

{% include documentation/5/tutorials/knowbook.md %}

Advanced Tutorials
------------------

  * [Remote Access](/documentation/5/tutorials/remote_access/): this tutorial covers how to enable remote access to the Postgresql database.

