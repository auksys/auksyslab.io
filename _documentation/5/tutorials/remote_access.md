---
title: Remote Access
classes: wide
---

{% include documentation/5/banner.md %}

**Disclaimer:** enabling remote access to the postgresql database is a *security risk*.

In general, it is not recommended to enable remote access to the Postgresql database used in kDB. But this can be necesserary for debugging purposes, especially if the database is running on a headless computer.
First, initialise the store (for more information check the [getting started tutorial](/documentation/5/tutorials/getting_started/)).

```bash
# The following will create a PostgreSQL database in 'path/to/' using
# the default port 1242:
kdb store --path path/to/
# Use Ctrl+C to stop the store.
```

Then open the file `path/to/postgresql.conf` and add the following at the end:

```
listen_addresses = '*'
```

Then open the file `path/to/pg_hba.conf`  and add the following at the end:

```
host    all             all             0.0.0.0/0            trust
```

Then restart the store. And you can now create remote connection to the Postgresql database:

<div class="tab-grey-background">

{% tabs connection %}

{% tab connection C++ %}
```c++
#include <kDB/Repository/Connection.h>

// If the database is remote, the same method for creating the connection can be used,
// for instance:
kDB::Repository::Connection connection = kDB::Repository::Connection::create("127.0.0.1", 1242);

// Once a connection is created, it is required to connect
// to a database with:
connection.connect();
```
{% endtab %}

{% tab connection Python %}
```python
from kDB.Repository import Connection

# If the database is remote, the same method for creating the connection can be used,
# for instance:
connection = Connection.create("127.0.0.1", 1242);

# Once a connection is created, it is required to connect to a database with:
connection.connect();
```
{% endtab %}

{% tab connection Ruby %}
```ruby
require 'kDB/Repository'

# If the database is remote, the same method for creating the connection can be used,
# for instance:
connection = KDB::Repository::Connection.create "127.0.0.1", 1242

# Once a connection is created, it is required to connect to a database with:
connection.connect;
```
{% endtab %}

{% endtabs %}

</div>
