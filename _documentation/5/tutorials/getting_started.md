---
title: Getting Started
classes: wide
---

{% include documentation/5/banner.md %}

In this tutorial, we will see how to start a kDB store, create a connection to one, and execute SQL/SPARQL queries.
This tutortial covers the usage of the kDB library as a stand alone component for integration in other frameworks and application.
Before starting with this tutorial, it is highly recommended to read the [overview](/documentation/5/libraries/kdb/) presentation of kDB. 

Running examples from this tutorial can be found at the [getting started examples](https://gitlab.com/auksys/auksys_examples/-/tree/main/getting_started).

Start a store
-------------

A store is a Postgres database. The kDB library handles the initialisation and manages the execution of the Postgres database. It also provides API for querying the database.
A store can be started and stopped from the API or the command line.

<div class="tab-grey-background">

{% tabs store %}

{% tab store C++ %}
```c++
#include <QDir>
#include <kDB/Repository/Store.h>

// The following will create a PostgreSQL database in 'path/to' using the default port 1242:
kDB::Repository::Store store(QDir("path/to"));

// The following will start the database, and initialize it if needed:
store.start();

// The following will shut down the database
store.stop();
```
{% endtab %}

{% tab store Python %}
```python
from kDB.Repository import Store

# The following will create a PostgreSQL database in 'path/to' using the default port 1242:
store = Store("path/to")

# The following will start the database, and initialize it if needed:
store.start()

# The following will shut down the database
store.stop()
```
{% endtab %}

{% tab store Ruby %}
```ruby
require 'kDB/Repository'

# The following will create a PostgreSQL database in 'path/to' using the default port 1242:
store = KDB::Repository::Store.new "path/to"

# The following will start the database, and initialize it if needed:
store.start

# The following will shut down the database
store.stop
```
{% endtab %}

{% tab store Cmd %}

```bash
# The following will create a PostgreSQL database in 'path/to/' using
# the default port 1242:
kdb store --path path/to/
# Use Ctrl+C to stop the store.
```

{% endtab %}

{% endtabs %}

</div>

Create a connection
--------------------

A connection is needed by a client to connect to a store.

<div class="tab-grey-background">

{% tabs connection %}

{% tab connection C++ %}
```c++
#include <kDB/Repository/Connection.h>

// If the store is created in the same program, as previously, a connection can simply
// be created with:
kDB::Repository::Connection connection = store.createConnection();

// Otherwise, assuming the directory of the store is 'path/to' and the port is 1242 it
// can be created with:
kDB::Repository::Connection connection = kDB::Repository::Connection::create("path/to", 1242);

// Once a connection is created, it is required to connect
// to a database with:
connection.connect();
```
{% endtab %}

{% tab connection Python %}
```python
from kDB.Repository import Connection

# If the store is created in the same program, as previously, a connection can simply
# be created with:
connection = store.createConnection()

# Otherwise, assuming the directory of the store is 'path/to' and the port is 1242 it can
# be created with:
connection = Connection.create("path/to", 1242);

# Once a connection is created, it is required to connect to a database with:
connection.connect()
```
{% endtab %}

{% tab connection Ruby %}
```ruby
require 'kDB/Repository'

# If the store is created in the same program, as previously, a connection can simply
# be created with:
connection = store.createConnection

# Otherwise, assuming the directory of the store is 'path/to' and the port is 1242 it
# can be created with:
connection = KDB::Repository::Connection.create "path/to", 1242

# Once a connection is created, it is required to connect to a database with:
connection.connect;
```
{% endtab %}

{% tab connection Cmd %}
No connection object is needed for the command line.
{% endtab %}

{% endtabs %}

</div>

Execute an SQL/SPARQL Query
---------------------------

Once a connection is established to a database, it is possible to execute SQL or SPARQL queries.

<div class="tab-grey-background">

{% tabs query %}

{% tab query C++ %}
```c++
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/RDFDataset.h>
#include <kDB/Repository/RDFEnvironment.h>

// Assuming a 'connection' was created.

// The following creates an SQL query, listing the content of the triples_stores table.
knowDBC::Query query = connection.createSQLQuery("SELECT * FROM triples_stores");

// The following creates an SPARQL query, listing the content of an RDF Graph.
kDB::Repository::RDFDataset info_rd = 
    connection.graphsManager()->getDataset("http://askco.re/graphs#info"_kCu).expectSuccess();
knowDBC::Query query = connection.createSPARQLQuery({info_rd}, "SELECT * WHERE { ?x ?y ?z. }");

// The following execute either the SQL or SPARQL query
knowDBC::Result result = query.execute();

// The following print the results on the terminal
for(int i = 0; i < result.tuples(); ++i)
{
  std::cout << i;
  for(int j = 0; j < result.fields(); ++j)
  {
    std::cout << " " << result.value(i, j).printable().expectSuccess().toStdString();
  }
  std::cout << std::endl;
}

```
{% endtab %}

{% tab query Python %}
```python
from kDB.Repository import RDFEnvironment
from knowCore import Uri
import knowRDF # This is needed to print literals

# Assuming a 'connection' was created.

# The following creates an SQL query, listing the content of the triples_stores table.
query = connection.createSQLQuery("SELECT * FROM triples_stores")

# The following creates an SPARQL query, listing the content of an RDF Graph.
info_ts = connection.graphsManager().getTriplesStore(Uri("http://askco.re/graphs#info"))
query = connection.createSPARQLQuery(
  RDFEnvironment(info_ts),
  "SELECT * WHERE { ?x ?y ?z. }")

# The following execute either the SQL or SPARQL query
result = query.execute()

# The following print the results on the terminal
for i in range(0, result.tuples()):
    print(str(i), end="")
    for j in range(0, result.fields()):
        print(f" {result.value(i, j)}", end="")
    print("")
```
{% endtab %}

{% tab query Ruby %}
```ruby
# Needed to handle literals, as results from SPARQL queries.
require 'knowRDF'

# Assuming a 'connection' was created.

# The following creates an SQL query, listing the content of the triples_stores table.
query = connection.createSQLQuery "SELECT * FROM triples_stores"

# The following creates an SPARQL query, listing the content of an RDF Graph.
info_ts = connection.graphsManager.getTriplesStore kCu("http://askco.re/graphs#info")
query = connection.createSPARQLQuery(
    KDB::Repository::RDFEnvironment.new(info_ts),
    "SELECT * WHERE { ?x ?y ?z. }")

# The following execute either the SQL or SPARQL query
result = query.execute

# The following print the results on the terminal
for i in 0...result.tuples
  print i
  for j in 0...result.fields()
    print " #{result.value(i, j)}"
  end
  puts
end
```
{% endtab %}

{% tab query Cmd %}

```bash
# The following executes an SQL query, listing the content of the triples_stores table.
kdb_query --path path/to --port 1242 --sql "SELECT * FROM triples_stores"

# The following creates an SPARQL query, listing the content of an RDF Graph.
kdb_query --path path/to --port 1242 --sparql --default-dataset "http://askco.re/graphs#info" "SELECT * WHERE { ?x ?y ?z. }"
```

{% endtab %}

{% endtabs %}

</div>

Next
----

The following tutorials expand on various aspect of kDB:

* {% include documentation/5/tutorials/advanced_querying.md %}
* {% include documentation/5/tutorials/triple_stores.md %}
* {% include documentation/5/tutorials/ros/getting_started.md %}
