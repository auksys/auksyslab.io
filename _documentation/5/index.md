---
title: auKsys Documentation
permalink: /documentation/5/
classes: wide
---

{% include documentation/5/banner.md %}

This is the general documentation of auKsys version 5.

* Start with the [installation instructions](/documentation/installation/).
* And then go through our [tutorials](/documentation/5/tutorials/).
* If you have been using <i>auKsys/4</i>, we refer you to our page for migrating [from auKsys/4](/documentation/5/from_4).

The documentation also contains general presentation of the libraries:

{% include documentation/5/libraries.md %}

...programs:

{% include documentation/5/programs.md %}

...general concepts:

{% include documentation/concepts.md %}

...the query languages:

{% include documentation/5/query_languages.md %}

...and of the ROS components:

{% include documentation/5/ros.md %}
