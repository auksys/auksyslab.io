---
title: A Brief Introduction To RDF
classes: wide
---

RDF
---

[RDF](https://www.w3.org/RDF/) (Resource Description Framework) is a standard model for data interchange ont the Web. It is also a useful representation for Semantic Knowledge on board of a robot and for multi agents systems.

![Basic RDF Triple](/assets/images/Basic_RDF_Graph.svg.png){: style="float: left" width="300"} In RDF, the knowledge is represented as a graph. Where nodes are either URI (Uniform Resource Identifier) or values, and edges can have one URI label. RDF Graphs are commonly encoded as `subject-predicate-object`, where `subject` and `object` are nodes, and `predicate` is the edge, as shown on the figure on the left. `subject` and `edge` are always URIs, while `object` can be a value or an URI.

The following show an example of encoding of a RDF Graph using the turtle file format:

```turtle
<http://example.org/subject> <http://example.org/predicate> <http://example.org/subject_and_object> .
<http://example.org/subject_and_object> <http://example.org/other_predicate> "A string value" .
```

In RDF the URI can be arbitrary, but it is highly advised to use URIs from established ontologies or from a domain under one's control.

A larger example of a Knowledge Graph can be seen in the graph bellow. It represents the information related to two glasses on top of a table. For simplicity, the URI are represented using text. The ellipitic nodes contains URI, while the square one contains value.

[![Knowlege Graph](/assets/images/KnowledgeGraph.png)](/assets/images/KnowledgeGraph.png)

This graph represent the information about three objects `ObjA`, `ObjB` and `ObjC`. The graph encodes information about each objects, such as their `color`, or which substance they they `contains` and the `quantity`. The graph also contains three `classes`, which are general abstraction describing the objects. `ClassA` describes the concept of a table, while `ObjA` contains the information for one specific instance of a table.

For more details, we direct the readers to [RDF 1.1 primer](https://www.w3.org/TR/rdf11-primer/), the official introduction to RDF.

Turtle
------

[Turtle](https://www.w3.org/TR/rdf12-turtle/) (Terse RDF Triple Language) is a syntax and file format for expressing data in the Resource Description Framework (RDF) data model. It was introduced as a replacement of the original RDF/XML format to offer a more human readable form.

A turtle file starts with a header which consits of `@base` and a list of `@prefix`. `@base` is used to specify relative uri, while `@prefix` is used to specified shortcuts that can be used in the file. For instance, for the graph of the previous section, we can use the following header:

```turtle
@base: <http://aksco.re/example/>
@prefix relations: <http://aksco.re/example/relations/>
@prefix properties: <http://aksco.re/example/properties/>
@prefix substances: <http://aksco.re/example/substances/>
@prefix classes: <http://aksco.re/example/classes/>
@prefix dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>
@prefix un:  <http://www.w3.org/2007/ont/unit#>
```

With such a prefix, we can write `<ObjA>` instead of `<http://aksco.re/example/ObjA>` and `classes:ClassA` instead of `<http://aksco.re/example/ClassA>`.

This allow to encode the previous graph as:

```turtle

<ObjA> a classes:ClassA;
       properties:color "white" .
<ObjB> a classes:ClassB;
       relations:on_top_of <ObjA> ;
       properties:color "red" ;
       properties:quantity [ dul:hasDataValue 10 ; dul:isClassifiedBy   un:cl ] ;
       relations:contains substances:SubstanceA .
<ObjC> a classes:ClassB;
       relations:on_top_of <ObjA> ;
       properties:color "transparent" ;
       properties:quantity [ dul:hasDataValue 20 ; dul:isClassifiedBy   un:cl ] ;
       relations:contains substances:SubstanceA .```
```

We have not added the classes to the previous turtle file, as they are usual stored in a different RDF Graph, called an Ontology (see bellow). `a` is a special shortcut for `http://www.w3.org/2000/01/rdf-schema#type` and indicates that the `subject` belongs to the class `object`, so `<ObjC> a classes:ClassB` indicates that `<ObjC>` belongs to the class `classes:ClassB`.

SPARQL
------

[SPARQL](https://www.w3.org/TR/sparql11-query/) (SPARQL Protocol and RDF Query Language) is a query language over an RDF Graph.

In general, a SPARQL query take the form of:

```sparql
SELECT ?var1 ?var2 ... WHERE { ?var1 a ?var2 . }
```

`?var1 ?var2...` are the results of the query while `?var1 a ?var2 ?var1 a ?var2` is the structure of the graph.

Lets assume that a user is looking for a glass that contains less than `20cl` of `SubstanceA`. Such a query can be represented with the following graph:

[![How to query?](/assets/images/HowToQuery.png){: style="display:block; margin-left:auto; margin-right:auto" }](/assets/images/HowToQuery.png)

Where `?` is the node we are looking for, and `*` represents any. In other word, we are looking for the glass `?` on top of any table `*`. Such a query in SPARQL can be encoded as:

```sparql
BASE: <http://aksco.re/example/>
PREFIX relations: <http://aksco.re/example/relations/>
PREFIX properties: <http://aksco.re/example/properties/>
PREFIX substances: <http://aksco.re/example/substances/>
PREFIX classes: <http://aksco.re/example/classes/>
PREFIX dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>
PREFIX un:  <http://www.w3.org/2007/ont/unit#>

SELECT ?glass WHERE {
  ?glass a classes:ClassB ;
         relations:contains substances:SubstanceA ;
         relations:on_top_of ?table ;
         properties:quantity [
           dul:hasDataValue ?quantity ;
           dul:isClassifiedBy un:cl ] .
  FILTER( ?quantity < 20 )
  ?table a classes:ClassA .
}
```

The structure of a SPARQL Query is very similar to a mix of Turtle and SQL. But essentially, `?identifier` indicate an unknown variable, and correspond to the `?` and `*` of the previous graph. `SELECT ?glass` indicates that we are only interested in the glass and not the other variables. The `?quantity` variable is used in the `FILTER` expression, as the graph statement of SPARQL can only be used to indicate equality.


OWL
---

[OWL](https://www.w3.org/OWL/) (Web Ontology Language) is a Semantic Web language designed to represent rich and complex knowledge about things, groups of things, and relations between things. Ontologies are used to define the meaning of the URI used in an RDF Graph, they encode the semantic of a graph.

An ontology is encoded as an RDF Graph, it is grounded in Descriptive Logic. The ontology gives meaning to the URI used as classes and edges. For instance, the following is a subset of the ontology for the two glasses and table example:

```turtle
@base <http://askco.re/example/ontology/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

@prefix relations: <http://aksco.re/example/relations/>
@prefix classes: <http://aksco.re/example/classes/>

<http://askco.re/example/ontology> rdf:type owl:Ontology ;
                           rdfs:comment "This ontology contains the terms and vocabulary used to describe the example." ;
                           rdfs:label "ask example ontology" .

#################################################################
#    Object Properties
#################################################################

relations:on_top_of rdf:type owl:ObjectProperty ;
                 rdfs:comment "This property indicates that the subject is on top of the object." ;
                 rdfs:label "on top of" .

#################################################################
#    Classes
#################################################################

classes:ClassA rdf:type owl:Class ;
           rdfs:comment "This indicates that the object is a table." ;
           rdfs:label "table" .
```

It describes the `relations:on_top_of` and the class `classes:ClassA`. This is the most basic ontology, as it is only giving a description and a label for each concepts. A much richer ontology would include relation between classes and properties.

SHACL
-----

[SHACL](https://www.w3.org/TR/shacl/) (Shapes Constraint Language) is a language that can be used to validate RDF graphs. In some sense, SHACL defines a schema for the graph structure of RDF. SHACL defines what nodes and edges combination are valid.

```turtle
@base: <http://askco.re/example/shacl>
@prefix sh: <http://www.w3.org/ns/shacl#> .

@prefix classes: <http://aksco.re/example/classes/>
@prefix properties: <http://aksco.re/example/properties/>
@prefix relations: <http://aksco.re/example/relations/>

<table_shape> a sh:NodeShape ;
  sh:targetClass classes:ClassA ;
  sh:property [
    rdfs:label "color" ;
    sh:path properties:color ;
    sh:datatype xsd:string
    sh:maxCount 1
  ] ;
  sh:property [
    sh:path [ sh:inversePath relations:on_top_of ] ;
    sh:nodeKind sh:IRI
  ] ;
  .
```

This indicates that a `table` can have at most one `color` property. And the `color` property is a string. Also a `table` can be the predicate in a `on_top_of` property, and the subject in such a relation is an URI, this was the relation indicating that glasses are on top of the table.

More information about SHACL can be found in [Validating RDF Data](http://book.validatingrdf.com/).

Further reading
---------------

Official specifications:

- [RDF 1.1 Concepts and Abstract Syntax](https://www.w3.org/TR/rdf11-concepts/)
- [RDF 1.1 Turtle](https://www.w3.org/TR/turtle/)
- [SPARQL 1.1 Query Language](https://www.w3.org/TR/sparql11-query/)

Tutorials and introductions:

- [RDF 1.1 primer](https://www.w3.org/TR/rdf11-primer/) the official introduction to RDF
- [Tutorial: Introduction to RDF and OWL by CSIRO](https://csiro-enviro-informatics.github.io/info-engineering/tutorials/tutorial-intro-to-rdf-and-owl.html)
- [Validating RDF Data](http://book.validatingrdf.com/) a book about SHACL