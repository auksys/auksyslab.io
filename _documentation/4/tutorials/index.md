---
title: Tutorials
permalink: /documentation/4/tutorials/
---

{% include documentation/4/banner.md %}

Basic Tutorials
---------------

* {% include documentation/4/tutorials/getting_started.md %}
* {% include documentation/4/tutorials/triples_stores.md %}
* {% include documentation/4/tutorials/advanced_querying.md %}
* {% include documentation/4/tutorials/base_knowledge.md %}

Data Tutorials
--------------

{% include documentation/4/tutorials/data.md %}

ROS Tutorials
-------------

{% include documentation/4/tutorials/ros.md %}

pralin/compose Tutorials
------------------------

{% include documentation/4/tutorials/pralin/compose.md %}

Advanced Tutorials
------------------

  * [Remote Access](/documentation/4/tutorials/remote_access/): this tutorial covers how to enable remote access to the Postgresql database.

