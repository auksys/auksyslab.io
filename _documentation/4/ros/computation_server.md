---
title: Computation Server
classes: wide
---

{% include documentation/4/banner.md %}

The computation server is a ROS node that can be used to run computations dyamically, and in particular for data stored in a `kDB` database.

computation_server node
-----------------------

`computation_server` is the main node for running computation. After starting, it offers the following services:

* `check_status` (`ros_pralin_interfaces/srv/CheckStatus`) it is used to check the state of a computation.
* `pause_processing` (`ros_pralin_interfaces/srv/ControlProcessing`) it is used to pause a computation.
* `resume_processing` (`ros_pralin_interfaces/srv/ControlProcessing`) it is used to resume a computation, after it has been paused.
* `start_processing` (`ros_pralin_interfaces/srv/StartProcessing`) it is used to start a new computation.
* `stop_processing` (`ros_pralin_interfaces/srv/ControlProcessing`) it is used to stop a computation.

Note that `pause_processing` and `stop_processing` can only interrupt a computation between two algorithms call. So if a computation is running for a long time in an algorithm, it won't be stopped.
`check_status`, `pause_processing`, `resume_processing` and `stop_processing` takes as an argument the UUID of the computation, the UUID was either given as an argument during `start_processing`, or returned in the response.

### start_processing

The `start_processing` service use the following arguments:

* `definition` (`string`): a YAML definition of the processing.
* `uuid` (`string`): uuid to identify the processing (optional, if not given, a UUID will be generated and returned).
* `name_space` (`string`): namespace to use for the topics created by the inputs/outputs. If not given, the topics are created in a namespace generated from the UUID.
* `parameters` (`string`): a YAML string defining the parameters used by the computation.
* `repeat` (`bool`): if true, the computation repeat on inputs, if false, it will ends once it has completed once.
* `inputs_triggers` (`string[]`): if empty, any input will trigger a computation, if set, only the specified inputs will
* `inputs_mappings` (`Mapping[]`): remap input topics
* `outputs_mappings` (`Mapping[]`): remap output topics

If the composition defined in `definition` has inputs or outputs, the `computation_server` will attempt to create topics for those inputs and outputs.
This require for the type of the input to be supported, otherwise, it will trigger an error.
The topic are created using the `name_space`, or if no namespace is specified, using the `uuid`.
Usually, in ROS, topics can be remapped when the node is started, but this is not possible with the computation server. This can be solved using the `inputs_mappings` and `output_mappings`. `Mapping` has the following fields:
* `connector_name` (`string`): name of the input/output, as defined in the `definition`.
* `topic_name` (`string`): name of the topic to re-map to. If the name starts with a `/`, it will be an absolute topic, otherwise it will be relative to the namespace of the server.
`

run_computation
---------------

`run_computation` is a program that can be used to start a new computation from the command line.

It has the following usage:

```bash
run_computation [options] pralin_compose.yaml
```

Where `pralin_compose.yaml` is a filename with a composition definition. Alternatively, the composition can be specified in the terminal when using the `--stdin` option.
The list of options are:

* `--help` shows the help message.
* `--stdin` read the composition definition from the terminal, instead of a file.
* `--repeat` repeat the calculation until it is terminated by a service call.
* `--not-waiting` by default, the script wait until the calculation is finished, used this option if you want the script to terminate as soon as the calculation is started.
* `--namespace ns` define the namespace used by the topics created by the computation, it default to the UUID of the calculation
* `--parameters params` a YAML string with the parameters used by the computation.

Example of use of this program can be found in the [ROS Computation Server tutorial](/documentation/4/tutorials/ros/computation_server/).

Related Tutorials
-----------------

* {% include documentation/4/tutorials/pralin/compose/introduction.md %}
* {% include documentation/4/tutorials/ros/computation_server.md %}
