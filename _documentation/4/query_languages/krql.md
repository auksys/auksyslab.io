---
title: "Knowledge Request Query Language"
classes: wide
---

{% include documentation/4/banner.md %}

The `krQL` language is used to execute request against a subset of the kDB API.

It relies on using YAML map of the form:

```yaml
key:
  pname1: pvalue1
  pname2: pvalue2
```

For instance, the following query returns the property `prop1` of object `a` from `test_ts`:

```
salient region:
  action: get property
  dataset: test_ts
  object: a
  property: prop1
```
