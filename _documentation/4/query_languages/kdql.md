---
title: "kDQL: kDB Document Query Language"
classes: wide
---

{% include documentation/4/banner.md %}

Since kDB 4.2, a new module for storing and querying JSON documents is available, via the `kDBDocuments` interface. This page describes the *kDQL* query language used by the module. kDQL queries are valid [YAML](https://yaml.org/) documents. In `kDBDocuments` the documents (also ref)

Basic structure
---------------

All queries are represented as an `YAML Map` with a single key (among `create`, `drop`, `retrieve`...).
It is possible to use bindings, with external value, using the `!binding` tag:

```YAML
%TAG ! tag:askco.re/kdql,2023:
retrieve:
  what: "*"
  from: tests
  matches:
    name: !binding name_value
```

`name_value` will be replaced by the value given in the bindings in query API call.

Creation Query
--------------

This query is used to add documents to a collection. The creation query uses the `create` key, it expects an `YAML Map` with two keys:
 - `into`: a string with the name of the collection where the `values` will be added. If the collection does not exists, it will be created.
 - `documents`: a list of documents that will be stored in the collection.

For instance, the following query add three documents into the `tests` collection:

```YAML
create:
  into: tests
  documents:
    - name: "a"
      age: 10
      location: "world"
    - name: "b"
      age: 20
      location: "universe"
    - name: "b"
      age: 30
      location: "world"
```

Sometime a string in a document might be too large to store directly in the query, and it is more convenient to store it in a seperate file. Then it is possible to use `!include` tag, as follow:

```YAML
%TAG ! tag:askco.re/kdql,2023:
- drop:
    - domains 
- create:
    into: domains
    values:
      - name: spot_domain
        pddl: !include spot_domain.pddl
```

This query assumes the existence of a `spot_domain.pddl` file that will be read and the value of the field `pddl` will be set to the content of the file.


Retrieve Query
--------------

This query is used to retrieve documents from a collection. The retrieve query uses the `retrieve` key, it expects an `YAML Map` with three keys:

 - `what`: either the string `"*"` or a list of string. If `what` is set to `"*"` then the matching documents are returned. If `what` is set to a list of string, then the value of those documents are returned in different columns.
 - `from`: a string with the name of the collection,
 - `matches`: a template describing how to match the documents. Currently kDQL only support equality.

The following query will retrieve all the documents with a field `name` equal to `"a"` from the `tests` collection:

```YAML
retrieve:
  what: "*"
  from: tests
  matches:
    name: "a"
```

The following query will retrieve returns the fields `name` and `age` of the documents with a field `name` equal to `"b"` and `location` equal to `"world"` from the `tests` collection:

```YAML
retrieve:
  what: [name, age]
  from: tests
  matches:
    name: "b"
    location: "world"
```

Drop Query
----------

This query is used to remove a collection of documents. The drop query uses the `drop` key, it expects a sequence of strings with the name of the collections to remove.

For instance, the following query will drop two collections (`collection 1` and `collection 2`):

```YAML
drop:
  - collection 1
  - collection 2
```

Composite Queries
-----------------

kDQL allows to execute multiple queries at once, then the root element of the YAML document should be an array.
For instance:

```YAML
- drop:
    - test 
- create:
    into: test
    documents:
      - name: hello
        location: world
```

This query will remove the `test` collection and create a new entry in the `test` collection with the given values.
