---
title: "SPARQL: SPARQL Protocol and RDF Query Language"
classes: wide
---

{% include documentation/4/banner.md %}

kDB implements a subset of the [SPARQL 1.1](https://www.w3.org/TR/sparql11-query/) specification, with some deviations with the standard regarding the handling of types.

Strongly typed
--------------

A main difference between a standard SPARQL and the kDB implementation is the handling of types. In a standard SPARQL, all literals are strings, and it is always possible to retrieve the original string representation of a literal. In kDB/SPARQL, literals use their native type, so that a number is represented as a number, for instance. However, the original string representation is lost, and when converting back to a string, it is not always possible to get back the original representation.

This allows kDB/SPARQL to be extended to handle binary data, such as images or point clouds.

List of supported features
--------------------------

Coming soon...

Among missing features is variadic paths. They are rather complicated to implement properly, and have performance issues. Implementations of variadic paths is very unlikely.
