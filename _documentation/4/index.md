---
title: auKsys Documentation
permalink: /documentation/4/
classes: wide
---

{% include documentation/4/banner.md %}

This is the general documentation of auKsys version 4.

* Start with the [installation instructions](/documentation/installation/).
* And then go through our [tutorials](/documentation/4/tutorials/).

The documentation also contains general presentation of the libraries:

{% include documentation/4/libraries.md %}

...general concepts:

{% include documentation/concepts.md %}

...the query languages:

{% include documentation/4/query_languages.md %}

...and of the ROS components:

{% include documentation/4/ros.md %}
