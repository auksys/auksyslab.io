---
title: knowL
classes: wide
---

{% include documentation/4/banner.md %}

Base library for knowledge.

Core Components
---------------

* `knowCore`: core libraries.
* `knowDataTransfert`: base library for implementing data transfert.
* `knowDBC`: Database Connection.
* `knowGIS`: Geographic Information Systems (GIS).
* `knowRDF`: RDF data.
* `knowSHACL`: SHACL implementation for a constraint language.
* `knowValues`: data structures sensor and data values.
* `knowVis`: 3D Knowledge Visualisation components.

Important links
---------------

* [API Documentation (stable)](https://lrs.gitlab-pages.liu.se/knowl/)
* [Source code](https://gitlab.com/auksys/knowl)
