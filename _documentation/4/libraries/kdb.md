---
title: kDB
classes: wide
---

{% include documentation/4/banner.md %}

The Knowledge DataBase (kDB) is a library that provides functionnalities for storing knowledge, from low-level sensor data to high-level semantic information.
kDB relies on PostgreSQL for storing the data. kDB provides API and tools for controlling and connecting to the server, storing and querying data.

Overview
--------

The figure below provides an overview of the library:

[![image overview of kDB](/assets/images/kdb_architecture.png)](/assets/images/kdb_architecture.png)

At the lowest level, data is stored in SQL Tables. kDB provides default schema for a number of type of data, it also allows to create custom tables by running custom SQL queries.
kDB also provdides triples stores, that can be queries using SPARQL and where the data can be imported from RDF files.

The kDB API allows access to the data through different mechanism, using SQL or SPARQL queries, Active Records or RDF files.
kDB also provides API for exchanging sensor data and automatic synchronisation of triple stores.

Clients are built on top of the API, several official clients are included in auKsys, such as the ROS server, an user interface (knowX), the computation server (pralin) and also an RDF compatible end-point.

Core Components
---------------

* `kDB/RDFDB`: light weight implementation of a database, for RDF database.
* `kDB/Repository`: main library for interacting with a kDB Store.
* `kDB/RDFView`: implementation of (Sparqlify)[https://github.com/SmartDataAnalytics/Sparqlify] for mapping SQL tables to RDF Triples and querying with SPARQL.
* `kDB/SPARQL`


Extensions
----------

* `kDBBaseKnowledge` allows to load base knowledge from a file system in the database.
* `kDBDatasets` support for storing datasets.
* `kDBDataSynchronisation` implementation of the dataset exchange protocol.
* `kDBDocuments` implementation of a JSON-based document storage and query engine.
* `kDBEndPoint` enables a SPARQL end-point.
* `kDBPointClouds` support for storing point cloud data in a kDB store.
* `kDBQueries` generates API for querying the database with pre-defined SPARQL queries.
* `kDBRDFGraphSynchronisation` implementation of th RDF Graph synchronisation protocol.
* `kDBSensing` support for storing sensor data in a kDB store.
* `kDBGIS` support for storing GIS data in a kDB store.
* `kDBMapping` support related to storing 3d maps in a kDB Store
* `kDBRobotics` support for storing information related to robots in a database.

Qt/QML binding libraries and plugins:

* `kDBQuick`
* `kDBGISQuick`
* `kDBDatasetsQuick`


Important links
---------------

* [API Documentation (stable)](https://lrs.gitlab-pages.liu.se/kdb/)
* [Source code](https://gitlab.com/auksys/kdb)
